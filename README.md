# Rebor

Modern editor, with IDE features, written in Rust. 

## Status

Proof of concept, as in:
  - little to no test
  - awfull error handling
  - Absolutely no API stability
  - No project management
  - Eats your data if you try to use it for anything serious.
  - no configuration, I hard-coded my preferences (most notably kakoune-like keybindings)
  - absolute bad-taste in UI theming.

Heck, even the name of the product will eventualy change. (it's a reboot of a previous project
of mine named `bor`, hence `rebor`. Ew. Ugly name)

## Develop

__Note:__ The TUI is broken at the moment, as I did some slight changes in the theming protocol.

```sh
git clone https://gitlab.com/lisael/rebor.git
cd rebor
# build and run the terminal ui
# cargo run --bin rebortui
# build and run the druid (graphical) ui
cargo run --bin reborgui
```

Tested only on Linux with the gtk druid backend.

### Architecture principles

The main goal is that the editor MUST always feel highly responsive and snappy. Any noticeable lag
in editor interactions is treated as a bug.

To achieve this, we try to find the fastest path from the keystroke or mouse movement
to the paint on screen. All the rest (highlighting, linting, formating...) is threaded using an
in-house actor model. Actors are simple threads that communicate through channels.


## Status

This is an informal todo list, easier to manage than individual issues as we move very fast.
As soon as we can test properly we start a more conventional dev workflow, with
issues, feature branches, unit tests, integration tests, proper doc, and merge requests.

### TODO/Known bugs

- [ ] TODO: use RopeSlices anywhere we manipulate the content (line iterators, `reborlib::theme::Chunk`...)
- [ ] TODO: command line should be sent as a chunk.
- [ ] TODO: use grapheme indices instead of chars (helix does this but it needs RopeSlices everywhere)
- [ ] TODO: the input handler should be a field in the ui rather than an actor
- [ ] BUG: There is a noticable slowdown when the cusror is at the end of a massive file (>50k lines)
  The viewport iterates on the lines and skips the part above the view. It's probably here. We should
  cut the buffer X lines before the view and iterate over this instead.
- [x] BUG: first offset when reaching the bottom of the current viewport is buggy
  (probably in view.rs as it's buggy both for TUI and GUI)
- [ ] BUG: manage character width (either small ligatures (ff, fi, ffi...) and wide graphemes)
- [ ] FACT: Ligatures hate you (almost as much as I hate them in code, so don't expect a fast resolution)
    - [ ] document how to disable them while we don't support them
    - [ ] add an option to enable/disable them when character width bug is fixed

### MVP (by priority/dependency)

The implementations of those features must keep in mind the next
batch (use channels to be client/server ready rather than function calls,
avoid Linux-only features to be cross-plaform, ...)

- [ ] TUI
    - [x] Status line
        - [x] Minimal support
        - [x] Editor status
            - [x] Edit mode
        - [x] Buffer status
            - [x] buffer name
            - [x] cursor pos
    - [x] Normal mode
    - [ ] Command mode
        - [x] Minimal support
        - [ ] Client side commands
          - [x] Minimal support
          - [x] `:q`
          - [ ] `:e`
          - [ ] `:w`
          - [ ] `:wq`
    - [x] Refactor the TUI using breeze-like buffer cache and diff.
    - [ ] Ovlerlay windows
        - [ ] Alert
        - [ ] Dialog
        - [ ] Diagnostics
    - [ ] RGB colors
- [x] GUI bootstrap (using druid. git@github.com:aDotInTheVoid/Wingspan.git for related work.)
  - [x] Basic editor widget
  - [x] Enclose in a proper Scroll widget
  - [x] Add line numbers
  - [x] Status line
- [x] Cursor movements
    - [x] `l`
    - [x] `h`
    - [x] `j`
    - [x] `k`
    - [x] `$` (EOL)
- [x] Insert transition
    - [x] `i`
    - [x] `I`
    - [x] `a`
    - [x] `A`
    - [x] `o`
    - [x] `O`
- [ ] Scrolling
    - [ ] On input
        - [x] Fix the strange behaviour on the last line.
    - [ ] On user request
        - [x] GUI
        - [ ] TUI
- [x] Colors
- [x] Show the cursor
- [x] Line numbers
- [ ] Buffer backend manager
  - [ ] stdin/stdout
    - [ ] load
    - [ ] write
  - [ ] FS
    - [ ] load
    - [ ] write
- [ ] Buffers nav
- [ ] Test framework
- [ ] Regression tests 
- [ ] Filetype detection
- [ ] Syntax highlighting
- [x] Multiple cursors/selections
- [ ] Core-side commands
    - [ ] command parsing
    - [ ] command def
- [ ] Undo/Redo tree
- [ ] Line Wrap
- [ ] Handle line endings (`\n` only at the moment)

### nice to have before starting anything larger

- [ ] documentation
- [ ] log/debug buffer
- [x] refactor the code in well bound crates and traits
- [ ] trace framework to measure the latency

### maybe... (no specific order)

- [ ] configuration framework
- [ ] Buffer backend manager
  - [ ] FS
    - [ ] create dir on write
    - [ ] watch
    - [ ] sudo
  - [ ] ssh
    - [ ] load
    - [ ] write
    - [ ] watch
    - [ ] sudo
- [ ] Wasm extensions
- [ ] Support grapheme clusters
- [ ] Set-options
- [ ] smart buffer list
- [ ] swap file (with state auto-save)
- [ ] LSP client
- [ ] Configuration
- [ ] Auto indent
- [ ] Rust language pack
- [ ] Python language pack
- [ ] Client/server
- [ ] TreeSitter code objects
- [ ] Better error handling
- [ ] fuzzy searcher
- [ ] Project support
- [ ] Smart fuzzy search (frecency – including git data –, imports in the current buffer)
- [ ] RipGrep integration
- [ ] Git integration
- [ ] File tree
- [ ] Paren matching
- [ ] parinfer
- [ ] rainbow parens

### far future

...or from contributions, as I don't even own a mac or a MSWin box.

- [ ] *BSD support
- [ ] Mac OSX support
- [ ] MS Windows support

## License

AGPL

## Acknowledgements

- [Wingspan](https://github.com/aDotInTheVoid/Wingspan) The druid editor widget started from here (it evolved a lot, since)
- Helix. Nothing at the moment, but I'll definitely loot large chunks of the tree-sitter integration, and maybe LSP too.
