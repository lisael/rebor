use std::thread::{spawn, JoinHandle};

use ropey::{Rope, RopeSlice};

use crossbeam_channel::{unbounded, Receiver, Sender};

use crate::message::Msg;
use crate::selection::Selections;
use crate::status::BufferStatus;
use crate::theming::{Chunk, ChunkContent, Face};

pub(crate) struct Viewport {
    ui_chan: Sender<Msg>,
    line_cache: usize,
    version: u64,
    content: Rope,
    text: Vec<Chunk>,
    selections: Chunk,
    client_size: (u16, u16),
    line_offset: usize,
}

impl Viewport {
    fn new(ui_chan: Sender<Msg>, line_cache: usize) -> Self {
        Self {
            ui_chan,
            line_cache,
            version: Default::default(),
            content: Default::default(),
            text: Default::default(),
            selections: Default::default(),
            client_size: Default::default(),
            line_offset: Default::default(),
        }
    }

    pub(crate) fn run(ui_chan: Sender<Msg>, line_cache: usize) -> (Sender<Msg>, JoinHandle<()>) {
        let (viewport_chan, viewport_recv) = unbounded();
        let mut viewport = Self::new(ui_chan, line_cache);
        let mainloop = { spawn(move || viewport.mainloop(viewport_recv)) };
        (viewport_chan, mainloop)
    }

    fn mainloop(&mut self, recv: Receiver<Msg>) {
        loop {
            let msg = recv.recv().unwrap();
            match msg {
                Msg::Quit => {
                    // self.quit();
                    break;
                }
                Msg::ViewPortUpdate(rope, version, selections, client_size, line_offset) => {
                    self.update_ui(rope, version, selections, client_size, line_offset)
                }
                // Msg::ViewPortUpdateContent(version, rope, client_size, line_offset) => {
                //     self.update_content()
                // },
                // _ => {
                //     dbg!(msg);
                // }
                _ => panic!("Unexpected {:?}", msg),
            }
        }
    }

    fn update_text(&mut self, rope: &mut Rope) {
        let cache_offset = self.line_offset.saturating_sub(self.line_cache);
        let mut start_pos = rope.line_to_char(cache_offset);
        let len_lines = rope.len_lines();
        let rope = rope.split_off(start_pos);
        self.text = rope
            .lines()
            // .skip(cache_offset)
            .take(self.client_size.1 as usize + self.line_cache)
            .map(|s| {
                let c = Chunk {
                    start: start_pos,
                    end: start_pos + s.len_chars(),
                    face: Face::Default,
                    content: ChunkContent::Str(String::from(s)),
                };
                start_pos += s.len_chars();
                c
            })
            .collect();
        // append EOF chunk at the end of the file if visible.
        if len_lines <= cache_offset + self.text.len() {
            let mut last_line = self.text.pop().unwrap();
            last_line = last_line.push(Chunk {
                start: last_line.end,
                end: last_line.end + 1,
                face: Face::Default,
                content: ChunkContent::EOF,
            });
            self.text.push(last_line);
            // dbg!(&self.text);
        }
    }

    fn update_selections(&mut self, selections: &Selections) -> bool {
        let mut result = vec![];
        let mut pos = 0;
        for s in selections {
            let start = s.get_first();
            if (start > pos) & (result.len() > 0) {
                result.push(Chunk {
                    start: pos,
                    end: start,
                    face: Face::None,
                    content: ChunkContent::Nop,
                })
            };
            if s.is_collapsed() {
                result.push(Chunk {
                    start,
                    end: start + 1,
                    face: {
                        if s.main {
                            Face::MainSelectionCollapsed
                        } else {
                            Face::SecondarySelectionCollapsed
                        }
                    },
                    content: ChunkContent::Nop,
                })
            } else if s.is_reversed() {
                result.push(Chunk {
                    start,
                    end: start + 1,
                    face: {
                        if s.main {
                            Face::MainSelectionCursor
                        } else {
                            Face::SecondarySelectionCursor
                        }
                    },
                    content: ChunkContent::Nop,
                });
                result.push(Chunk {
                    start: start + 1,
                    end: s.anchor,
                    face: {
                        if s.main {
                            Face::MainSelectionBody
                        } else {
                            Face::SecondarySelectionBody
                        }
                    },
                    content: ChunkContent::Nop,
                });
            } else {
                result.push(Chunk {
                    start,
                    end: s.cursor - 1,
                    face: {
                        if s.main {
                            Face::MainSelectionBody
                        } else {
                            Face::SecondarySelectionBody
                        }
                    },
                    content: ChunkContent::Nop,
                });
                result.push(Chunk {
                    start: s.cursor - 1,
                    end: s.cursor,
                    face: {
                        if s.main {
                            Face::MainSelectionCursor
                        } else {
                            Face::SecondarySelectionCursor
                        }
                    },
                    content: ChunkContent::Nop,
                });
            }
            pos = s.get_second() + 1;
        }
        let selections = Chunk {
            start: result[0].start,
            end: result.last().unwrap().end,
            face: Default::default(),
            content: ChunkContent::Chunks(result),
        };
        if self.selections != selections {
            self.selections = selections;
            true
        } else {
            false
        }
    }

    fn merge(&self) -> Vec<Chunk> {
        self.text
            .iter()
            .map(|chunk| chunk.merge(&self.selections))
            .collect()
    }

    fn update_ui(
        &mut self,
        rope: Rope,
        version: u64,
        selections: Selections,
        client_size: (u16, u16),
        line_offset: usize,
    ) {
        let mut changed = false;
        if version != self.version {
            self.content = rope.clone();
            changed = true;
        }
        if (version != self.version)
            | (client_size != self.client_size)
            | (line_offset != self.line_offset)
        {
            self.client_size = client_size;
            self.line_offset = line_offset;
            self.update_text(&mut rope.clone());
            self.version = version;
            changed = true;
        }
        // dbg!(&selections);
        changed |= self.update_selections(&selections);
        if !changed {
            return;
        }
        // dbg!(&self.text);
        // dbg!(&self.selections);
        let content = self.merge();
        // dbg!(&content);
        let cursor = usize::min(selections.main().cursor, rope.len_chars());
        let cursor_line = rope.char_to_line(cursor);
        let cursor_col = cursor - rope.line_to_char(cursor_line);
        let max_line_length = content.iter().map(|c| c.len()).fold(0, |max, x| x.max(max));
        let bs = BufferStatus {
            buf_name: "*scratch*".to_owned(),
            buf_line_length: rope.len_lines(),
            client_offset: line_offset,
            cursor_line,
            cursor_col,
            content,
            max_line_length,
        };
        self.ui_chan.send(Msg::BufferStatus(bs)).unwrap();
    }
}
