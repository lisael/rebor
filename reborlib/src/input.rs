use std::thread::{spawn, JoinHandle};

use crossbeam_channel::{unbounded, Receiver, Sender};

use super::message::Msg;

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum EditorMode {
    Normal,
    Insert,
    Command,
    Goto,
}

impl Default for EditorMode {
    fn default() -> Self {
        EditorMode::Normal
    }
}

#[derive(Debug)]
pub enum Key {
    Ctrl(char),
    Alt(char),
    Char(char),
    Shift(Box<Key>),
    Up,
    Down,
    Left,
    Right,
    Backspace,
    Escape,
}

pub enum Input {
    Key(Key),
    Text(String),
    ClientSize(u16, u16),
    SetOffset(usize),
    SetCursorPos(usize, usize), // line, column
    Quit,
}

pub struct InputHandler {
    ui_chan: Sender<Msg>,
    core_chan: Sender<Msg>,
    current_view: usize,
}

impl InputHandler {
    fn new(core_chan: Sender<Msg>, ui_chan: Sender<Msg>) -> Self {
        Self {
            ui_chan,
            core_chan,
            current_view: 0,
        }
    }

    pub fn run(core_chan: Sender<Msg>, ui_chan: Sender<Msg>) -> (Sender<Input>, JoinHandle<()>) {
        let mut input = InputHandler::new(core_chan, ui_chan);
        let (input_chan, input_recv) = unbounded();
        let mainloop = spawn(move || input.normal_mode(input_recv));
        (input_chan, mainloop)
    }

    pub fn signal_mode(&mut self, mode: EditorMode) {
        self.ui_chan.send(Msg::SetEditorMode(mode)).ok();
    }

    pub fn normal_mode(&mut self, recv: Receiver<Input>) {
        self.signal_mode(EditorMode::Normal);
        loop {
            match recv.recv().unwrap() {
                Input::Quit => break,
                Input::ClientSize(w, h) => self.core_msg(Msg::ClientSize(self.current_view, w, h)),
                Input::SetOffset(o) => self.core_msg(Msg::SetOffset(self.current_view, o)),
                Input::SetCursorPos(l, c) => {
                    self.core_msg(Msg::SetCursorPos(self.current_view, l, c))
                }
                Input::Text(s) => {}
                Input::Key(k) => {
                    // eprintln!("{:?}", k);
                    match k {
                        Key::Char('i') => {
                            self.edit_mode(recv.clone());
                            self.signal_mode(EditorMode::Normal);
                        }
                        Key::Char('I') => {
                            self.core_msg(Msg::MoveToSOL(self.current_view, false));
                            self.edit_mode(recv.clone());
                            self.signal_mode(EditorMode::Normal);
                        }
                        Key::Char(':') => {
                            self.command_mode(recv.clone());
                            self.signal_mode(EditorMode::Normal);
                        }
                        Key::Char('a') => {
                            self.core_msg(Msg::MoveRight(self.current_view, false));
                            self.edit_mode(recv.clone());
                            self.signal_mode(EditorMode::Normal);
                        }
                        Key::Char('A') => {
                            self.core_msg(Msg::MoveToEOL(self.current_view, false));
                            self.edit_mode(recv.clone());
                            self.signal_mode(EditorMode::Normal);
                        }
                        Key::Char('o') => {
                            self.core_msg(Msg::Atomic(
                                self.current_view,
                                vec![
                                    Msg::MoveToEOL(self.current_view, false),
                                    Msg::InsertAtCursor(self.current_view, '\n'),
                                ],
                            ));
                            self.edit_mode(recv.clone());
                            self.signal_mode(EditorMode::Normal);
                        }
                        Key::Char('O') => {
                            self.core_msg(Msg::Atomic(
                                self.current_view,
                                vec![
                                    Msg::MoveToSOL(self.current_view, false),
                                    Msg::InsertAtCursor(self.current_view, '\n'),
                                    Msg::MoveLeft(self.current_view, false),
                                ],
                            ));
                            self.edit_mode(recv.clone());
                            self.signal_mode(EditorMode::Normal);
                        }
                        Key::Char('d') => self.core_msg(Msg::Delete(self.current_view)),
                        Key::Left | Key::Char('h') => {
                            self.core_msg(Msg::MoveLeft(self.current_view, false))
                        }
                        Key::Char('H') => self.core_msg(Msg::MoveLeft(self.current_view, true)),
                        Key::Right | Key::Char('l') => {
                            self.core_msg(Msg::MoveRight(self.current_view, false))
                        }
                        Key::Char('L') => self.core_msg(Msg::MoveRight(self.current_view, true)),
                        Key::Up | Key::Char('k') => {
                            self.core_msg(Msg::MoveUp(self.current_view, false))
                        }
                        Key::Char('K') => self.core_msg(Msg::MoveUp(self.current_view, true)),
                        Key::Down | Key::Char('j') => {
                            self.core_msg(Msg::MoveDown(self.current_view, false))
                        }
                        Key::Char('J') => self.core_msg(Msg::MoveDown(self.current_view, true)),
                        Key::Char('$') => self.core_msg(Msg::MoveToEOL(self.current_view, false)),
                        Key::Char('^') => self.core_msg(Msg::MoveToSOL(self.current_view, false)),
                        Key::Char('C') => self.core_msg(Msg::CopySelectionDown(self.current_view)),
                        Key::Char('g') => {
                            self.goto_mode(recv.clone());
                            self.signal_mode(EditorMode::Normal);
                        }
                        _ => (),
                    }
                }
            }
        }
    }

    pub fn goto_mode(&mut self, recv: Receiver<Input>) {
        self.signal_mode(EditorMode::Goto);
        loop {
            match recv.recv().unwrap() {
                Input::ClientSize(w, h) => self.core_msg(Msg::ClientSize(self.current_view, w, h)),
                Input::SetOffset(o) => self.core_msg(Msg::SetOffset(self.current_view, o)),
                Input::SetCursorPos(l, c) => {
                    self.core_msg(Msg::SetCursorPos(self.current_view, l, c))
                }
                Input::Key(k) => match k {
                    Key::Escape => break,
                    Key::Char('g') => {
                        self.core_msg(Msg::SetCursorPos(self.current_view, 0, 0));
                        break;
                    }
                    _ => (break),
                },
                _ => (),
            }
        }
    }

    pub fn core_msg(&mut self, msg: Msg) {
        self.core_chan.send(msg).unwrap();
    }

    pub fn edit_mode(&mut self, recv: Receiver<Input>) {
        self.signal_mode(EditorMode::Insert);
        loop {
            match recv.recv().unwrap() {
                Input::ClientSize(w, h) => self.core_msg(Msg::ClientSize(self.current_view, w, h)),
                Input::SetOffset(o) => self.core_msg(Msg::SetOffset(self.current_view, o)),
                Input::SetCursorPos(l, c) => {
                    self.core_msg(Msg::SetCursorPos(self.current_view, l, c))
                }
                Input::Text(s) => self.core_msg(Msg::InsertTextAtCursor(self.current_view, s)),
                Input::Key(k) => match k {
                    Key::Escape => break,
                    Key::Char(c) => self.core_msg(Msg::InsertAtCursor(self.current_view, c)),
                    Key::Backspace => self.core_msg(Msg::Backspace(self.current_view)),
                    Key::Left => self.core_msg(Msg::MoveLeft(self.current_view, false)),
                    Key::Right => self.core_msg(Msg::MoveRight(self.current_view, false)),
                    Key::Up => self.core_msg(Msg::MoveUp(self.current_view, false)),
                    Key::Down => self.core_msg(Msg::MoveDown(self.current_view, false)),
                    _ => (),
                },
                _ => (),
            }
        }
    }

    pub fn parse_command(&mut self, cmd: String) {
        if let Some(cmdname) = cmd.split_ascii_whitespace().next() {
            match cmdname {
                "q" | "quit" => self.core_msg(Msg::Quit),
                _ => (),
            }
        }
    }

    pub fn command_mode(&mut self, recv: Receiver<Input>) {
        self.signal_mode(EditorMode::Command);
        let mut command = "".to_owned();
        let mut cmd_pos: usize = 0;
        loop {
            match recv.recv().unwrap() {
                Input::ClientSize(w, h) => self.core_msg(Msg::ClientSize(self.current_view, w, h)),
                Input::SetOffset(o) => self.core_msg(Msg::SetOffset(self.current_view, o)),
                Input::Key(k) => match k {
                    Key::Escape => break,
                    Key::Char(c) => match c {
                        '\n' => {
                            self.ui_chan
                                .send(Msg::SetEditorCommand(String::from(""), 0))
                                .unwrap();
                            self.parse_command(command);
                            break;
                        }
                        _ => {
                            command.push(c);
                            cmd_pos += 1;
                            self.ui_chan
                                .send(Msg::SetEditorCommand(command.clone(), cmd_pos))
                                .unwrap()
                        }
                    },
                    Key::Backspace => {
                        if !command.is_empty() {
                            command.pop();
                            cmd_pos -= 1;
                            self.ui_chan
                                .send(Msg::SetEditorCommand(command.clone(), cmd_pos))
                                .unwrap()
                        }
                    }
                    _ => (),
                },
                _ => (),
            }
        }
    }
}
