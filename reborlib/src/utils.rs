/// compute the length of the decimal representation of a usize
pub fn num_digit(n: usize) -> usize {
    (f64::from(n as u32).ln() / (10.0f64).ln()).floor() as usize + 1
}
