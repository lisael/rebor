use crate::message::Msg;
use core::ops::{Bound, RangeBounds};
use crossbeam_channel::Sender;
use std::{
    path::PathBuf,
    sync::{Arc, RwLock, RwLockReadGuard, RwLockWriteGuard},
};

use ropey::Rope;

#[derive(Clone, Debug)]
pub enum EditKind {
    Insert,
    Delete,
}

#[derive(Clone, Debug)]
pub struct Edit {
    pub pos: usize,
    pub len: usize,
    pub kind: EditKind,
    pub version: u64,
}

pub struct Buffer {
    content: Rope,
    pub version: u64,
    watchers: Vec<Sender<Msg>>,
}

impl Buffer {
    pub fn new(location: Option<PathBuf>) -> Self {
        Self {
            content: Rope::new(),
            version: 0,
            watchers: vec![],
        }
    }

    pub fn insert_char(&mut self, char_idx: usize, ch: char) -> Edit {
        self.content.insert_char(char_idx, ch);
        self.version += 1;
        let e = Edit {
            pos: char_idx,
            len: 1,
            kind: EditKind::Insert,
            version: self.version,
        };
        self.notify(&e);
        e
    }

    pub fn insert(&mut self, char_idx: usize, text: &str) -> Edit {
        self.content.insert(char_idx, text);
        self.version += 1;
        let e = Edit {
            pos: char_idx,
            len: text.chars().count(),
            kind: EditKind::Insert,
            version: self.version,
        };
        self.notify(&e);
        e
    }

    pub fn remove<R>(&mut self, char_range: R) -> Edit
    where
        R: RangeBounds<usize>,
    {
        let start = match char_range.start_bound() {
            Bound::Included(s) => s.clone(),
            _ => panic!("Only bounded Ranges are accepted in Buffer::remove(range)"),
        };
        let end = match char_range.end_bound() {
            Bound::Excluded(s) => s.clone(),
            _ => panic!("Only bounded Ranges are accepted in Buffer::remove(range)"),
        };
        self.content.remove(char_range);
        self.version += 1;
        let e = Edit {
            pos: end,
            len: end - start,
            kind: EditKind::Delete,
            version: self.version,
        };
        self.notify(&e);
        e
    }

    pub fn content(&self) -> Rope {
        self.content.clone()
    }

    pub fn len_chars(&self) -> usize {
        self.content.len_chars()
    }

    pub fn len_lines(&self) -> usize {
        self.content.len_lines()
    }

    pub fn char_to_line(&self, char_idx: usize) -> usize {
        self.content.char_to_line(char_idx)
    }

    pub fn line_to_char(&self, char_idx: usize) -> usize {
        self.content.line_to_char(char_idx)
    }

    pub fn watch(&mut self, chan: Sender<Msg>) {
        self.watchers.push(chan);
    }

    pub fn notify(&self, e: &Edit) {
        for w in &self.watchers {
            w.send(Msg::BufferEdit(self.version, e.clone()));
        }
    }
}

#[derive(Clone)]
pub struct BufferHandle {
    buf: Arc<RwLock<Buffer>>,
}

impl BufferHandle {
    pub fn new(location: Option<PathBuf>) -> Self {
        Self {
            buf: Arc::new(RwLock::new(Buffer::new(location))),
        }
    }

    pub fn write(&self) -> RwLockWriteGuard<'_, Buffer> {
        return self.buf.write().unwrap();
    }

    pub fn read(&self) -> RwLockReadGuard<'_, Buffer> {
        return self.buf.read().unwrap();
    }

    pub fn content(&self) -> Rope {
        let buf = self.read();
        buf.content()
    }

    pub fn len_chars(&self) -> usize {
        let buf = self.read();
        buf.len_chars()
    }

    pub fn watch(&self, chan: Sender<Msg>) {
        let mut buf = self.write();
        buf.watch(chan);
    }
}
