use std::collections::HashMap;

#[derive(Debug, Clone, PartialEq, Eq, Hash, Default)]
pub enum Face {
    #[default]
    None,
    Default,
    LineNumber,
    Whitespace,
    InsertCursor,
    SelectCursor,
    HiddenCursor,
    MainSelectionCursor,
    MainSelectionBody,
    MainSelectionCollapsed,
    SecondarySelectionCursor,
    SecondarySelectionBody,
    SecondarySelectionCollapsed,
    Composed(Vec<Face>),
}

impl Face {
    pub fn compose(&self, other: &Face) -> Self {
        use Face::*;
        match self {
            None => other.clone(),
            Composed(faces) => match other {
                None => self.clone(),
                Composed(o_faces) => {
                    Composed(faces.iter().chain(o_faces.iter()).cloned().collect())
                }
                o_face => {
                    let mut faces: Vec<Face> = faces.iter().cloned().collect();
                    faces.push(o_face.clone());
                    Composed(faces)
                }
            },
            face => match other {
                None => self.clone(),
                Composed(o_faces) => {
                    let mut o_faces: Vec<Face> = o_faces.iter().cloned().collect();
                    let mut faces = vec![face.clone()];
                    faces.append(&mut o_faces);
                    Composed(faces)
                }
                o_face => Composed(vec![face.clone(), o_face.clone()]),
            },
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum StyleModifier {
    Bold,
    Concealed,
    CrossedOut,
    Faint,
    Framed,
    Invert,
    Italic,
    Reset,
    Underline,
}

impl Default for StyleModifier {
    fn default() -> Self {
        StyleModifier::Reset
    }
}

#[derive(Clone, Debug, Default, PartialEq)]
pub struct Style<C> {
    pub fg: Option<C>,
    pub bg: Option<C>,
    pub style: Option<StyleModifier>,
}

impl<C: Clone> Style<C> {
    pub fn paintover(mut self, other: &Self) -> Self {
        if other.fg.is_some() {
            self.fg = other.fg.clone();
        }

        if other.bg.is_some() {
            self.bg = other.bg.clone();
        }
        if other.style.is_some() {
            self.style = other.style.clone();
        }
        self
    }

    pub fn invert(&mut self) -> Self {
        Self {
            fg: self.bg.clone(),
            bg: self.fg.clone(),
            style: self.style.clone(),
        }
    }
}

pub struct StyleMap<C> {
    map: HashMap<Face, Style<C>>,
}

impl<C: Clone + std::fmt::Debug + PartialEq> StyleMap<C> {
    pub fn from_map(map: HashMap<Face, Style<C>>) -> Self {
        Self { map: map.clone() }
    }

    pub fn get(&self, face: &Face) -> Style<C> {
        match self.map.get(face) {
            Some(s) => s.clone(),
            None => self
                .map
                .get(&Face::Default)
                .expect("Face::Default must be present in the StyleMap")
                .clone(),
        }
    }

    pub fn get_or(&self, face: &Face, default: &Style<C>) -> Style<C> {
        self.map.get(face).unwrap_or(default).clone()
    }

    pub fn get_composite(&self, faces: &Vec<Face>) -> Style<C> {
        let mut style = self.get(&Face::Default);
        for face in faces {
            self.map
                .get(&face)
                .map(|s| style = style.clone().paintover(&s));
        }
        style
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Default)]
pub enum ChunkContent {
    Empty,
    #[default]
    Nop,
    EOF,
    Str(String),
    Chunks(Vec<Chunk>),
}

#[derive(Clone, Debug, PartialEq, Eq, Default)]
pub struct Chunk {
    pub start: usize,
    pub end: usize,
    pub face: Face,
    pub content: ChunkContent,
}

pub struct ChunkCharsIterator<'a> {
    chunks: ChunksIterator<'a>,
    current_chars: Option<std::str::Chars<'a>>,
}

impl<'a> ChunkCharsIterator<'a> {
    pub fn new(chunk: &'a Chunk) -> Self {
        Self {
            chunks: chunk.into_iter(),
            current_chars: None,
        }
    }
}

impl<'a> Iterator for ChunkCharsIterator<'a> {
    type Item = char;

    fn next(&mut self) -> Option<Self::Item> {
        match self.current_chars {
            Some(ref mut chars) => {
                let next = chars.next();
                match next {
                    Some(_) => return next,
                    None => {
                        self.current_chars = None;
                        self.next()
                    }
                }
            }
            None => match self.chunks.next() {
                None => None,
                Some(next_chunk) => {
                    if let ChunkContent::Str(s) = &next_chunk.content {
                        self.current_chars = Some(s.chars());
                    };
                    self.next()
                }
            },
        }
    }
}

/// Iterate over a Chunk, recursively flattening compound Chunks. (i.e
/// `ChunkContent::Chunks(childre) = chunk.content`
///
/// TODO: we should compose the face of flattened Chunks with the
///       parent chunk's face.
pub struct ChunksIterator<'a> {
    chunk: Option<&'a Chunk>,
    current_iter: Option<Box<ChunksIterator<'a>>>,
    current_chunks: Option<std::slice::Iter<'a, Chunk>>,
    other_chunks: Option<&'a [Chunk]>,
}

impl<'a> ChunksIterator<'a> {
    fn new(chunk: &'a Chunk) -> Self {
        Self {
            chunk: Some(chunk),
            current_iter: None,
            current_chunks: None,
            other_chunks: None,
        }
    }
}

impl<'a> Iterator for ChunksIterator<'a> {
    type Item = &'a Chunk;

    fn next(&mut self) -> Option<&'a Chunk> {
        match &self.chunk {
            None => None,
            Some(c) => match &c.content {
                ChunkContent::Chunks(chunks) => {
                    if self.current_chunks.is_none() {
                        self.current_chunks = Some(chunks.into_iter());
                    };
                    match &mut self.current_iter {
                        None => match self.other_chunks {
                            None => {
                                self.other_chunks = Some(&chunks);
                                self.next()
                            }
                            Some(ref mut chunks) => {
                                if chunks.len() == 0 {
                                    None
                                } else {
                                    let next_chunk = &chunks[0];
                                    *chunks = &chunks[1..];
                                    self.current_iter = Some(Box::new(next_chunk.into_iter()));
                                    self.next()
                                }
                            }
                        },
                        Some(iter) => match iter.next() {
                            None => {
                                self.current_iter = None;
                                self.next()
                            }
                            Some(c) => Some(c),
                        },
                    }
                }
                _ => self.chunk.take(),
            },
        }
    }
}

impl<'a> IntoIterator for &'a Chunk {
    type Item = &'a Chunk;
    type IntoIter = ChunksIterator<'a>;

    fn into_iter(self) -> ChunksIterator<'a> {
        ChunksIterator::new(self)
    }
}

impl Chunk {
    pub fn chars(&self) -> ChunkCharsIterator<'_> {
        ChunkCharsIterator::new(self)
    }

    pub fn contains(&self, pos: usize) -> bool {
        (pos >= self.start) & (pos < self.end)
    }

    pub fn len(&self) -> usize {
        self.end - self.start
    }

    pub fn push(&self, new: Self) -> Self {
        match &self.content {
            ChunkContent::Chunks(chunks) => {
                let mut new_content = chunks.clone();
                let end = new.end;
                new_content.push(new);
                Chunk {
                    start: self.start,
                    end,
                    face: self.face.clone(),
                    content: ChunkContent::Chunks(new_content),
                }
            }
            _ => {
                let mut new_content = vec![self.clone()];
                let end = new.end;
                new_content.push(new);
                Chunk {
                    start: self.start,
                    end,
                    face: Default::default(),
                    content: ChunkContent::Chunks(new_content),
                }
            }
        }
    }

    pub fn slice(&self, start: usize, end: usize) -> Self {
        let mut result = vec![];
        let mut pos = usize::max(start, self.start);
        use ChunkContent::*;
        match &self.content {
            Chunks(chunks) => {
                for c in chunks {
                    if pos >= end {
                        break;
                    }
                    if c.end > pos {
                        let new = c.slice(pos, usize::min(c.end, end));
                        pos = new.end;
                        result.push(new);
                    }
                }
            }
            Str(s) if self.end > pos => {
                let end = usize::min(self.end, end);
                let mut new = self.clone();
                new.start = pos;
                new.end = end;
                new.content = Str(s
                    .chars()
                    .skip(new.start - self.start)
                    .take(new.end - new.start)
                    .collect());
                return new;
            }
            Nop if self.end > pos => {
                let end = usize::min(self.end, end);
                let mut new = self.clone();
                new.start = pos;
                new.end = end;
                return new;
            }
            _ => {}
        }
        match result.len() {
            0 => Chunk {
                start: self.start,
                end: self.start,
                face: Face::None,
                content: Nop,
            },
            1 => result.pop().unwrap(),
            _ => Chunk {
                start: result[0].start,
                end: result.last().unwrap().end,
                face: Face::None,
                content: Chunks(result),
            },
        }
    }

    pub fn merge(&self, other: &Self) -> Self {
        let other = other.slice(self.start, self.end);
        use ChunkContent::*;

        if let Nop = other.content {
            if other.face == Face::None {
                return self.clone();
            }
        };
        let mut result = vec![];
        // dbg!(&self);
        // dbg!(&other);
        for this in self {
            // dbg!(&result);
            let mut start = this.start;
            // dbg!(&this);
            for that in &other {
                if start >= that.end {
                    continue;
                }
                // dbg!(start);
                // dbg!(&that);
                // that.start = 0
                // that.end = 1
                // this.start = 1
                // this.end = 2
                // start = 1
                if that.start >= this.end {
                    break;
                }
                if !that.contains(start) {
                    result.push(this.slice(start, that.start));
                    start = that.start;
                }
                let end = usize::min(this.end, that.end);
                let mut new = this.slice(start, end);
                new.face = new.face.compose(&that.face);
                result.push(new);
                start = end;
            }
            if start < this.end {
                result.push(this.slice(start, this.end))
            }
        }
        // dbg!(&result);
        match result.len() {
            0 => Chunk {
                start: self.start,
                end: self.start,
                face: Face::None,
                content: Nop,
            },
            1 => result.pop().unwrap(),
            _ => Chunk {
                start: result[0].start,
                end: result.last().unwrap().end,
                face: Face::None,
                content: Chunks(result),
            },
        }
    }

    pub fn merge_old(&self, other: &Self) -> Self {
        use ChunkContent::*;
        let mut result: Vec<Chunk> = match &self.content {
            Chunks(chunks) => chunks.iter().map(|chunk| chunk.merge(other)).collect(),
            Str(s) => {
                let mut pos = self.start;
                let prelude = if self.start < other.start {
                    let end = usize::min(self.end, other.start);
                    pos = end;
                    vec![Chunk {
                        start: self.start,
                        end,
                        face: self.face.clone(),
                        content: Str(s.chars().take(end - self.start).collect()),
                    }]
                } else {
                    vec![]
                };
                match &other.content {
                    Chunks(chunks) => {
                        let mut res: Vec<Chunk> = prelude
                            .iter()
                            .cloned()
                            .chain(chunks.iter().filter_map(|c| {
                                if c.contains(pos) {
                                    let end = usize::min(self.end, other.end);
                                    let res = Some(Chunk {
                                        start: pos,
                                        end,
                                        face: self.face.compose(&c.face),
                                        content: Str(s.chars().skip(pos).take(end - pos).collect()),
                                    });
                                    pos = end;
                                    res
                                } else {
                                    None
                                }
                            }))
                            .collect();
                        if pos < self.end {
                            res.push(Chunk {
                                start: pos,
                                end: self.end,
                                face: self.face.clone(),
                                content: Str(s.chars().skip(pos).take(self.end - pos).collect()),
                            });
                        }
                        res
                    }
                    _ => vec![],
                }
            }
            _ => vec![],
        };
        match result.len() {
            0 => Chunk {
                start: self.start,
                end: self.end,
                face: Face::None,
                content: Nop,
            },
            1 => result.pop().unwrap(),
            _ => Chunk {
                start: self.start,
                end: self.end,
                face: Face::None,
                content: Chunks(result),
            },
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_chars() {
        let start = 0;
        let end = 0;
        let face = Face::None;
        let a = Chunk {
            start,
            end,
            face: face.clone(),
            content: ChunkContent::Str("a".to_owned()),
        };
        assert_eq!(a.chars().collect::<String>(), "a");
        let b = Chunk {
            start,
            end,
            face: face.clone(),
            content: ChunkContent::Str("b".to_owned()),
        };
        let c = Chunk {
            start,
            end,
            face: face.clone(),
            content: ChunkContent::Chunks(vec![a.clone(), b.clone()]),
        };
        assert_eq!(c.chars().collect::<String>(), "ab");
        let ca = Chunk {
            start,
            end,
            face: face.clone(),
            content: ChunkContent::Chunks(vec![c.clone(), a.clone()]),
        };
        assert_eq!(ca.chars().collect::<String>(), "aba");
        let bc = Chunk {
            start,
            end,
            face: face.clone(),
            content: ChunkContent::Chunks(vec![b.clone(), c.clone()]),
        };
        assert_eq!(bc.chars().collect::<String>(), "bab");
        let cc = Chunk {
            start,
            end,
            face: face.clone(),
            content: ChunkContent::Chunks(vec![c.clone(), c.clone()]),
        };
        assert_eq!(cc.chars().collect::<String>(), "abab");
    }
    #[test]
    fn test_iter() {
        let start = 0;
        let end = 0;
        let face = Face::None;
        let a = Chunk {
            start,
            end,
            face: face.clone(),
            content: ChunkContent::Str("a".to_owned()),
        };
        assert_eq!(a.into_iter().collect::<Vec<&Chunk>>(), vec![&a]);
        let b = Chunk {
            start,
            end,
            face: face.clone(),
            content: ChunkContent::Str("b".to_owned()),
        };
        let c = Chunk {
            start,
            end,
            face: face.clone(),
            content: ChunkContent::Chunks(vec![a.clone(), b.clone()]),
        };
        assert_eq!(c.into_iter().collect::<Vec<&Chunk>>(), vec![&a, &b]);
        let ca = Chunk {
            start,
            end,
            face: face.clone(),
            content: ChunkContent::Chunks(vec![c.clone(), a.clone()]),
        };
        assert_eq!(ca.into_iter().collect::<Vec<&Chunk>>(), vec![&a, &b, &a]);
        let bc = Chunk {
            start,
            end,
            face: face.clone(),
            content: ChunkContent::Chunks(vec![b.clone(), c.clone()]),
        };
        assert_eq!(bc.into_iter().collect::<Vec<&Chunk>>(), vec![&b, &a, &b]);
        let cc = Chunk {
            start,
            end,
            face: face.clone(),
            content: ChunkContent::Chunks(vec![c.clone(), c.clone()]),
        };
        assert_eq!(
            cc.into_iter().collect::<Vec<&Chunk>>(),
            vec![&a, &b, &a, &b]
        );
    }
    #[test]
    fn test_slice() {
        let c = Chunk {
            start: 0,
            end: 5,
            face: Face::Default,
            content: ChunkContent::Str("12345".to_owned()),
        };
        assert_eq!(
            c.slice(2, 4),
            Chunk {
                start: 2,
                end: 4,
                face: Face::Default,
                content: ChunkContent::Str("34".to_owned())
            }
        );
        let c2 = Chunk {
            start: 5,
            end: 10,
            face: Face::Default,
            content: ChunkContent::Str("67890".to_owned()),
        };
        let c3 = Chunk {
            start: 0,
            end: 10,
            face: Face::None,
            content: ChunkContent::Chunks(vec![c.clone(), c2.clone()]),
        };
        assert_eq!(c3.slice(0, 10), c3);
        assert_eq!(
            c3.slice(2, 8),
            Chunk {
                start: 2,
                end: 8,
                face: Face::None,
                content: ChunkContent::Chunks(vec![
                    Chunk {
                        start: 2,
                        end: 5,
                        face: Face::Default,
                        content: ChunkContent::Str("345".to_owned())
                    },
                    Chunk {
                        start: 5,
                        end: 8,
                        face: Face::Default,
                        content: ChunkContent::Str("678".to_owned())
                    },
                ])
            }
        );
        let c4 = Chunk {
            start: 0,
            end: 1,
            face: Face::None,
            content: ChunkContent::Chunks(vec![Chunk {
                start: 0,
                end: 1,
                face: Face::MainSelectionCollapsed,
                content: ChunkContent::Nop,
            }]),
        };
        assert_eq!(
            c4.slice(0, 1),
            Chunk {
                start: 0,
                end: 1,
                face: Face::MainSelectionCollapsed,
                content: ChunkContent::Nop,
            }
        );
    }
}
