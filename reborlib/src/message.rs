use super::buffer::Edit;
use super::input::EditorMode;
use super::selection::Selections;
use super::status::BufferStatus;

use ropey::Rope;

#[derive(Debug, Clone)]
pub enum Msg {
    Tick,
    Error(String),
    Quit,
    CoreOpenBuffer(String, bool),
    InsertAtCursor(usize, char),
    InsertTextAtCursor(usize, String),
    Backspace(usize),
    ViewInsertAtCursor(char),
    ViewBackspace,
    ClientSize(usize, u16, u16), // view_id, width, hieght
    BufferStatus(BufferStatus),
    SetEditorMode(EditorMode),
    SetEditorCommand(String, usize), // command, cursor pos in command
    MoveLeft(usize, bool),           // view_id, expand
    MoveRight(usize, bool),          // view_id, expand
    MoveUp(usize, bool),             // view_id, expand
    MoveDown(usize, bool),           // view_id, expand
    MoveToEOL(usize, bool),          // view_id, expand
    MoveToSOL(usize, bool),          // view_id, expand
    Delete(usize),                   // view_id
    ViewPortUpdate(Rope, u64, Selections, (u16, u16), usize), // content, version, selections, view size, line_offset
    Reload,
    NotChanged,
    NewContent(Rope),
    ShowView(usize),
    SetOffset(usize, usize),           // view_id, offset
    SetCursorPos(usize, usize, usize), // view_id, line, col
    Atomic(usize, Vec<Msg>), // view_id, [atomic view messages]. Use this when the view should ensure changes are atomic
    BufferEdit(u64, Edit),   // version, edit
    CopySelectionDown(usize),
}
