use crate::buffer::{Edit, EditKind};
use std::borrow::{Borrow, BorrowMut};

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct Selection {
    pub anchor: usize,
    pub cursor: usize,
    pub virtual_column: Option<usize>,
    pub main: bool,
    pub version: u64,
}

impl Selection {
    pub(crate) fn collapsed(pos: usize, main: bool) -> Self {
        Self {
            anchor: pos,
            cursor: pos,
            virtual_column: None,
            main,
            version: 0,
        }
    }

    pub fn len(&self) -> usize {
        self.get_second() - self.get_first()
    }

    pub fn is_collapsed(&self) -> bool {
        self.anchor == self.cursor
    }

    pub fn is_reversed(&self) -> bool {
        self.anchor > self.cursor
    }

    pub fn contains(&self, pos: usize) -> bool {
        if self.is_reversed() {
            (self.cursor <= pos) & (self.anchor >= pos)
        } else {
            (self.cursor >= pos) & (self.anchor <= pos)
        }
    }

    pub fn get_first(&self) -> usize {
        if self.is_reversed() {
            self.cursor
        } else {
            self.anchor
        }
    }

    pub fn get_second(&self) -> usize {
        if self.is_reversed() {
            self.anchor
        } else {
            self.cursor
        }
    }

    pub fn set_first(&mut self, pos: usize) {
        if self.is_reversed() {
            self.cursor = pos;
        } else {
            self.anchor = pos;
        }
    }

    pub fn set_second(&mut self, pos: usize) {
        if self.is_reversed() {
            self.anchor = pos;
        } else {
            self.cursor = pos;
        }
    }

    pub fn move_first(&mut self, len: i64) {
        let bound = self.get_first() as i64;
        self.set_first((bound + len) as usize)
    }

    pub fn move_second(&mut self, len: i64) {
        let bound = self.get_second() as i64;
        self.set_second((bound + len) as usize)
    }

    pub(crate) fn apply_edit(&mut self, edit: &Edit) -> bool {
        if self.version >= edit.version {
            return false;
        }
        self.version = edit.version;
        self.virtual_column = None;
        if self.is_collapsed() {
            if edit.pos <= self.anchor {
                match edit.kind {
                    EditKind::Insert => {
                        self.anchor += edit.len;
                        self.cursor = self.anchor;
                    }
                    EditKind::Delete => {
                        self.anchor -= edit.len;
                        self.cursor = self.anchor;
                    }
                }
                true
            } else {
                false
            }
        } else if self.contains(edit.pos) {
            match edit.kind {
                EditKind::Insert => {
                    self.move_second(edit.len as i64);
                }
                EditKind::Delete => {
                    let end_pos = edit.pos - edit.len;
                    if end_pos > self.get_first() {
                        self.move_second(0 - edit.len as i64)
                    } else {
                        self.set_first(end_pos);
                        self.set_second(usize::max(end_pos, self.get_second() - edit.len));
                    }
                }
            }
            true
        } else if self.anchor > edit.pos {
            match edit.kind {
                EditKind::Insert => {
                    self.anchor += edit.len;
                    self.cursor += edit.len;
                }
                EditKind::Delete => {
                    self.anchor -= edit.len;
                    self.cursor -= edit.len;
                }
            }
            true
        } else {
            false
        }
    }
}

#[derive(Debug, Clone)]
pub struct Selections(pub Vec<Selection>);

impl IntoIterator for Selections {
    type Item = Selection;
    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> std::vec::IntoIter<Self::Item> {
        self.0.into_iter()
    }
}

impl<'s> IntoIterator for &'s Selections {
    type Item = &'s Selection;
    type IntoIter = std::slice::Iter<'s, Selection>;

    fn into_iter(self) -> std::slice::Iter<'s, Selection> {
        self.0[..].into_iter()
    }
}

impl Selections {
    pub(crate) fn iter_mut(&mut self) -> std::slice::IterMut<'_, Selection> {
        self.0[..].iter_mut()
    }

    pub(crate) fn len(&self) -> usize {
        self.0.len()
    }

    pub(crate) fn sort(&mut self) {
        self.0.sort()
    }

    // TODO: probably a premature optimization
    pub(crate) unsafe fn get_unchecked(&self, idx: usize) -> &Selection {
        self.0.get_unchecked(idx)
    }

    pub(crate) fn push(&mut self, s: Selection) {
        if s.main {
            self.main_mut().main = false;
        }
        self.0.push(s);
        self.sort();
    }

    fn for_pos(&self, pos: usize) -> Option<&Selection> {
        for s in &self.0 {
            if s.contains(pos) {
                return Some(&s);
            }
        }
        None
    }

    pub(crate) fn main_mut(&mut self) -> &mut Selection {
        for s in self.0.iter_mut() {
            if s.main {
                return s;
            }
        }
        unreachable!("There must be one and only one main selection")
    }

    pub(crate) fn main(&self) -> &Selection {
        for s in &self.0 {
            if s.main {
                return s;
            }
        }
        unreachable!("There must be one and only one main selection")
    }
}

impl Borrow<[Selection]> for Selections {
    fn borrow(&self) -> &[Selection] {
        &self.0[..]
    }
}

impl BorrowMut<[Selection]> for Selections {
    fn borrow_mut(&mut self) -> &mut [Selection] {
        &mut self.0[..]
    }
}
