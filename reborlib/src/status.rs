use super::theming::Chunk;

#[derive(Debug, Default, Clone, PartialEq, Eq)]
pub struct BufferStatus {
    pub buf_name: String,
    pub buf_line_length: usize,
    pub client_offset: usize,
    pub cursor_line: usize,
    pub cursor_col: usize,
    pub content: Vec<Chunk>,
    pub max_line_length: usize,
}
