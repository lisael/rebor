use std::thread::{spawn, JoinHandle};

use crossbeam_channel::{unbounded, Receiver, Sender};

use super::buffer::{Buffer, BufferHandle, Edit, EditKind};
use super::message::Msg;
use crate::selection::{Selection, Selections};
use crate::viewport::Viewport;

pub struct View {
    buffer: BufferHandle,
    selections: Selections,
    // viewport: Viewport,
    ui_chan: Sender<Msg>,
    visible: bool,
    client_size: (u16, u16),
    line_offset: usize,
    viewport: Option<Sender<Msg>>,
    line_cache: usize,
    version: u64,
}

// TODO: This becomes messy. Find a way to organize this better.
// TODO: Be sure to limit the usage of the buffer's RWLock
impl View {
    pub fn new(buffer: BufferHandle, ui_chan: Sender<Msg>, line_cache: usize) -> Self {
        Self {
            buffer,
            selections: Selections(vec![Selection::collapsed(0, true)]),
            // viewport: Viewport::new(ui_chan.clone()),
            visible: true,
            ui_chan,
            client_size: (0, 0),
            line_offset: 0,
            viewport: None,
            line_cache,
            version: 0,
        }
    }

    pub fn run(
        buffer: BufferHandle,
        ui_chan: Sender<Msg>,
        line_cache: usize,
    ) -> (Sender<Msg>, JoinHandle<()>) {
        let (view_chan, view_recv) = unbounded();
        let mut view = View::new(buffer.clone(), ui_chan, line_cache);
        let mainloop = { spawn(move || view.mainloop(view_recv)) };
        buffer.watch(view_chan.clone());
        (view_chan, mainloop)
    }

    fn handle_message(&mut self, msg: Msg, should_update: bool) -> bool {
        let must_update = match msg {
            Msg::Atomic(_, msgs) => {
                let mut update = false;
                for msg in msgs {
                    update |= self.handle_message(msg, false);
                }
                update
            }
            Msg::InsertAtCursor(_, c) => self.insert_at_cursor(c),
            Msg::InsertTextAtCursor(_, t) => self.insert_text_at_cursor(&t),
            Msg::ShowView(_) => self.show(),
            Msg::Backspace(_) => self.backspace(),
            Msg::ClientSize(_, w, h) => self.set_client_size(w, h),
            Msg::MoveLeft(_, expand) => self.move_left(expand),
            Msg::MoveRight(_, expand) => self.move_right(expand),
            Msg::MoveUp(_, expand) => self.move_up(expand),
            Msg::MoveDown(_, expand) => self.move_down(expand),
            Msg::MoveToEOL(_, expand) => self.move_cursor_to_eol(expand),
            Msg::MoveToSOL(_, expand) => self.move_cursor_to_sol(expand),
            Msg::SetOffset(_, o) => self.set_offset(o),
            Msg::SetCursorPos(_, l, c) => self.set_cursor_pos(l, c),
            Msg::BufferEdit(v, e) => self.apply_edit(e),
            Msg::CopySelectionDown(_) => self.copy_selection_down(),
            Msg::Delete(_) => self.delete_selections(),
            _ => {
                dbg!(msg);
                false
            } // _ => panic!("Unexpected {:?}", msg),
        };
        match (should_update, must_update) {
            (true, true) => {
                self.update_ui();
                false
            }
            (false, true) => true,
            (_, false) => false,
        }
    }

    fn mainloop(&mut self, recv: Receiver<Msg>) {
        loop {
            let msg = recv.recv().unwrap();
            match msg {
                Msg::Quit => {
                    self.quit();
                    break;
                }
                msg => {
                    self.handle_message(msg, true);
                }
            }
        }
    }

    fn apply_edit(&mut self, edit: Edit) -> bool {
        if edit.version > self.version {
            self.version = edit.version;
            self.foreach_selection(|this, s| this.selection_apply_edit(s, &edit))
        } else {
            false
        }
    }

    fn selection_apply_edit(&self, sel: &mut Selection, edit: &Edit) -> bool {
        sel.apply_edit(edit)
    }

    fn copy_selection_down(&mut self) -> bool {
        let main = self.selections.main();
        let buffer = self.buffer.read();
        let len_chars = buffer.len_chars();
        let len_lines = buffer.len_lines();
        let line = buffer.char_to_line(main.anchor);
        let col = main.anchor - buffer.line_to_char(line);
        let mut new_line = line;
        loop {
            new_line += 1;
            if new_line >= (len_lines) {
                return false;
            }
            let pos = buffer.line_to_char(new_line) + col;
            if pos > len_chars {
                return false;
            }
            if buffer.char_to_line(pos) == new_line {
                let mut new_sel = Selection::collapsed(pos, true);
                self.update_offset(line, new_line, &mut new_sel);
                self.selections.push(new_sel);
                return true;
            }
        }
    }

    /// Reset all selections and create one at pos
    fn set_cursor_pos(&mut self, l: usize, c: usize) -> bool {
        let content = self.buffer.content();
        if content.len_chars() == 0 {
            return false;
        }
        let old_line = content.char_to_line(self.selections.main().cursor);
        let pos = if l >= content.len_lines() {
            content.len_chars()
        } else {
            let pos = content.line_to_char(l);
            let line = content.line(l);
            if c >= line.len_chars() {
                let pos = pos + line.len_chars();
                if pos > content.len_chars() {
                    content.len_chars()
                } else if content.char(pos - 1) == '\n' {
                    pos - 1
                } else {
                    pos
                }
            } else {
                pos + c
            }
        };
        if (self.selections.len() == 1)
            & (unsafe {
                let sel = self.selections.get_unchecked(0);
                sel.is_collapsed() & (sel.cursor == pos)
            })
        {
            false
        } else {
            let mut sel = Selection::collapsed(pos, true);
            let new_line = content.char_to_line(pos);
            self.line_offset = self.update_offset(old_line, new_line, &mut sel);
            self.selections = Selections(vec![sel]);
            true
        }
    }

    fn set_offset(&mut self, o: usize) -> bool {
        if self.line_offset != o {
            self.line_offset = o;
            true
        } else {
            false
        }
    }

    // TODO implement the hide pendent method and Msg
    fn show(&mut self) -> bool {
        self.viewport = Some(Viewport::run(self.ui_chan.clone(), self.line_cache).0);
        true
    }

    /// Move the cursor and update the line offset and the virtual column if needed.
    /// If the new cursor is past the last char of the buffer, it's wrapped to the last char.
    fn move_selection_cursor_to_char(
        &mut self,
        selection: &mut Selection,
        new_cursor: usize,
        expanding: bool,
    ) -> bool {
        let max_pos = self.buffer.len_chars();
        let new_cursor = usize::min(new_cursor, max_pos);
        let old_cursor = selection.cursor;

        if new_cursor == old_cursor {
            return false;
        };
        let was_collapsed = selection.is_collapsed();
        selection.cursor = new_cursor;
        if !expanding {
            selection.anchor = new_cursor;
        } else if was_collapsed & (selection.len() == 1) {
            if selection.is_reversed() {
                selection.anchor = (selection.anchor + 1).min(max_pos);
                if selection.len() == 1 {
                    selection.anchor = selection.cursor;
                    return false;
                }
            } else {
                selection.cursor = (selection.cursor + 1).min(max_pos);
                if selection.len() == 1 {
                    selection.cursor = selection.anchor;
                    return false;
                }
            }
        }

        let orig_line = self.curline(old_cursor);
        let new_line = self.curline(new_cursor);
        self.line_offset = self.update_offset(orig_line, new_line, selection);
        true
    }

    fn update_offset(&self, orig_line: usize, new_line: usize, selection: &mut Selection) -> usize {
        #[allow(clippy::comparison_chain)]
        if orig_line == new_line {
            selection.virtual_column = None;
            self.line_offset
        } else if selection.main {
            if (new_line < orig_line) & (new_line <= self.line_offset + 3) {
                new_line.saturating_sub(3)
            } else if (new_line > orig_line)
                & (new_line >= (self.line_offset + self.client_size.1 as usize).saturating_sub(3))
            {
                (new_line + 3).saturating_sub(self.client_size.1 as usize)
            } else {
                self.line_offset
            }
        } else {
            self.line_offset
        }
    }

    fn move_cursor_to_eol(&mut self, expand: bool) -> bool {
        self.foreach_selection(|this, sel| {
            let curline = this.curline(sel.cursor);
            let content = this.buffer.content();
            let len = this.line_length(curline);
            let mut pos = content.line_to_char(curline) + len;
            // on the last line there is no new line so the cursor is before the last char
            if content.len_lines() == curline + 1 {
                pos += 1
            }
            this.move_selection_cursor_to_char(sel, pos, false)
        })
    }

    fn move_cursor_to_sol(&mut self, expand: bool) -> bool {
        self.foreach_selection(|this, s| {
            let curline = this.curline(s.cursor);
            let pos = this.buffer.content().line_to_char(curline);
            this.move_selection_cursor_to_char(s, pos, false)
        })
    }

    fn foreach_selection<F>(&mut self, mut f: F) -> bool
    where
        F: FnMut(&mut Self, &mut Selection) -> bool,
    {
        let mut result = false;
        let mut selections = std::mem::replace(&mut self.selections, Selections(vec![]));
        for s in selections.iter_mut() {
            result |= f(self, s)
        }
        self.selections = selections;
        result
    }

    fn edit_selections<F>(&mut self, mut f: F) -> bool
    where
        F: FnMut(&mut Selection, &mut Buffer) -> Edit,
    {
        let mut result = false;
        let mut selections = std::mem::replace(&mut self.selections, Selections(vec![]));
        let mut buffer = self.buffer.write();
        let mut edits: Vec<Edit> = vec![];
        for s in selections.iter_mut() {
            for e in &edits {
                self.selection_apply_edit(s, e);
            }
            let edit = f(s, &mut buffer);
            if edit.len != 0 {
                result = true;
                let orig_line = buffer
                    .content()
                    .char_to_line(s.cursor.min(buffer.len_chars()));
                self.selection_apply_edit(s, &edit);
                let new_line = buffer.content().char_to_line(s.cursor);
                self.line_offset = self.update_offset(orig_line, new_line, s);
                self.version = edit.version;
                edits.push(edit);
            } else {
                self.version = edit.version;
            }
        }
        self.selections = selections;
        result
    }

    fn insert_at_cursor(&mut self, c: char) -> bool {
        self.edit_selections(|sel, buffer| buffer.insert_char(sel.cursor, c))
    }

    fn insert_text_at_cursor(&mut self, t: &str) -> bool {
        self.edit_selections(|sel, buffer| buffer.insert(sel.cursor, t))
    }

    fn backspace(&mut self) -> bool {
        self.edit_selections(|sel, buffer| {
            let pos = sel.cursor;
            buffer.remove(pos.saturating_sub(1)..pos)
        })
    }

    fn cursor_forward(&mut self, range: usize, expanding: bool) -> bool {
        self.foreach_selection(|this, s| {
            this.move_selection_cursor_to_char(s, s.cursor + range, expanding)
        })
    }

    fn delete_selections(&mut self) -> bool {
        self.edit_selections(|s, buffer| {
            if s.is_collapsed() {
                let end = (s.cursor + 1).min(buffer.len_chars());
                buffer.remove(s.cursor..end)
            } else {
                buffer.remove(s.get_first()..s.get_second())
            }
        })
    }

    fn selection_cursor_backward(
        &mut self,
        sel: &mut Selection,
        range: usize,
        expanding: bool,
    ) -> bool {
        self.move_selection_cursor_to_char(sel, sel.cursor.saturating_sub(range), expanding)
    }

    fn cursor_backward(&mut self, range: usize, expand: bool) -> bool {
        self.foreach_selection(|this, s| this.selection_cursor_backward(s, range, expand))
    }

    fn update_ui(&mut self) {
        if let Some(vp) = &self.viewport {
            vp.send(Msg::ViewPortUpdate(
                self.buffer.content(),
                self.version,
                self.selections.clone(),
                self.client_size,
                self.line_offset,
            ))
            .unwrap()
        }
    }

    fn set_client_size(&mut self, w: u16, h: u16) -> bool {
        if self.client_size != (w, h) {
            self.client_size = (w, h);
            true
        } else {
            false
        }
    }

    fn move_left(&mut self, expand: bool) -> bool {
        self.cursor_backward(1, expand)
    }

    fn move_right(&mut self, expand: bool) -> bool {
        self.cursor_forward(1, expand)
    }

    fn move_up(&mut self, expand: bool) -> bool {
        self.foreach_selection(|this, sel| {
            let content = this.buffer.content();
            let mut curline = content.char_to_line(sel.cursor);
            if curline == 0 {
                return false;
            }
            let mut curcol = sel.cursor - content.line_to_char(curline);
            curline -= 1;
            let line_length = content.line(curline).len_chars().saturating_sub(1);
            let target_col = {
                if let Some(virtcol) = sel.virtual_column {
                    curcol = virtcol
                };
                if curcol > line_length {
                    sel.virtual_column = Some(curcol);
                    line_length
                } else {
                    curcol
                }
            };
            this.move_selection_cursor_to_char(
                sel,
                content.line_to_char(curline) + target_col,
                expand,
            );
            true
        })
    }

    fn main_line(&self) -> usize {
        self.buffer
            .content()
            .char_to_line(self.selections.main().cursor)
    }

    fn curline(&mut self, cursor: usize) -> usize {
        self.buffer.content().char_to_line(cursor)
    }

    fn curcol(&mut self, cursor: usize) -> usize {
        cursor - self.buffer.content().line_to_char(self.curline(cursor))
    }

    fn line_length(&mut self, lnr: usize) -> usize {
        self.buffer
            .content()
            .line(lnr)
            .len_chars()
            .saturating_sub(1)
    }

    fn move_down(&mut self, expand: bool) -> bool {
        self.foreach_selection(|this, sel| {
            let mut curline = this.curline(sel.cursor);
            let content = this.buffer.content();
            if curline == content.len_lines() - 1 {
                return false;
            }
            let mut curcol = this.curcol(sel.cursor);
            curline += 1;
            let line_length = this.line_length(curline);
            let target_col = {
                if let Some(virtcol) = sel.virtual_column {
                    curcol = virtcol
                };
                if curcol > line_length {
                    sel.virtual_column = Some(curcol);
                    line_length
                } else {
                    curcol
                }
            };
            this.move_selection_cursor_to_char(
                sel,
                content.line_to_char(curline) + target_col,
                expand,
            );
            true
        })
    }

    fn quit(&mut self) {
        if let Some(vp) = &self.viewport {
            vp.send(Msg::Quit).unwrap()
        }
    }
}
