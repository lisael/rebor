use super::message::Msg;
use std::path::PathBuf;
use std::thread::{spawn, JoinHandle};

use blake2::digest::generic_array::typenum::U64;
use blake2::digest::generic_array::GenericArray;
use blake2::{Blake2b, Digest};
use crossbeam_channel::{unbounded, Receiver, Sender};
use ropey::Rope;
use std::fs::File;
use std::io::{BufReader, BufWriter};

type HashType = GenericArray<u8, U64>;

pub struct Loader {
    location: Option<PathBuf>,
    buffer_chan: Sender<Msg>,
    hash: Option<HashType>,
}

impl Loader {
    fn new(buffer_chan: Sender<Msg>, location: Option<PathBuf>) -> Self {
        Self {
            location,
            buffer_chan,
            hash: None,
        }
    }

    fn hash_rope(&mut self, r: Rope) -> HashType {
        // TODO: stream the rope by chunks in the hasher to avoid memory
        //       duplication
        let bts = r.bytes().collect::<Vec<u8>>();
        Blake2b::digest(&bts)
    }

    pub fn run(
        buffer_chan: Sender<Msg>,
        location: Option<PathBuf>,
    ) -> (Sender<Msg>, JoinHandle<()>) {
        let mut input = Loader::new(buffer_chan, location);
        let (loader_chan, loader_recv) = unbounded();
        let mainloop = spawn(move || input.mainloop(loader_recv));
        loader_chan.send(Msg::Reload).unwrap();
        (loader_chan, mainloop)
    }

    fn mainloop(&mut self, recv: Receiver<Msg>) {
        loop {
            use super::message::Msg::*;
            match recv.recv().unwrap() {
                Quit => break,
                Reload => self.reload(),
                _ => (),
            }
        }
    }

    fn buffer_send(&self, msg: Msg) {
        self.buffer_chan.send(msg).unwrap();
    }

    fn open_location(&self, loc: PathBuf) -> Msg {
        match File::open(loc) {
            Ok(buf) => {
                let content = Rope::from_reader(BufReader::new(buf));
                match content {
                    Ok(r) => Msg::NewContent(r),
                    Err(err) => match err.kind() {
                        std::io::ErrorKind::InvalidData => {
                            Msg::Error("Not a UTF-8 file".to_owned())
                        }
                        _ => Msg::Error(err.to_string()),
                    },
                }
            }
            Err(_) => {
                let content = Rope::new();
                Msg::NewContent(content)
            }
        }
    }

    fn reload(&mut self) {
        let msg = match self.hash {
            None => match self.location.clone() {
                None => {
                    let content = Rope::new();
                    Msg::NewContent(content)
                }
                Some(loc) => self.open_location(loc),
            },
            // TODO: check the hash
            Some(_) => match self.location.clone() {
                None => Msg::NotChanged,
                Some(loc) => self.open_location(loc),
            },
        };
        match msg {
            Msg::NotChanged | Msg::Error(_) => self.buffer_send(msg),
            Msg::NewContent(r) => {
                self.hash = Some(self.hash_rope(r.clone()));
                self.buffer_send(Msg::NewContent(r))
            }
            _ => unreachable!(),
        }
    }
}
