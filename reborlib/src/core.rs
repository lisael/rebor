use std::marker::PhantomData;
use std::thread::{spawn, JoinHandle};

use crossbeam_channel::{unbounded, Receiver, Sender};

use crate::buffer::BufferHandle;
use crate::message::Msg;
// use crate::tui::UI;
use crate::view::View;

pub struct Core<UI> {
    views: Vec<Sender<Msg>>,
    view_handles: Vec<JoinHandle<()>>,
    ui_chan: Sender<Msg>,
    ui_handle: Option<JoinHandle<()>>,
    _phantom: PhantomData<UI>,
}

pub trait UI {
    fn run(chan: Sender<Msg>) -> (Sender<Msg>, JoinHandle<()>);
    fn line_cache() -> usize {
        0
    }
    fn scratch_text() -> String {
        "*** This is a scratch buffer ***".to_owned()
    }
}

impl<U: 'static> Core<U>
where
    U: UI + std::marker::Send,
{
    pub fn new(chan: Sender<Msg>) -> Self {
        // Create the UI
        let (ui_chan, ui_handler) = U::run(chan);
        Self {
            views: vec![],
            view_handles: vec![],
            ui_chan,
            ui_handle: Some(ui_handler),
            _phantom: PhantomData,
        }
    }

    /// Instanciate a Core in a thread and consume commands
    pub fn run() -> JoinHandle<()> {
        // Main core commands channel
        let (core_chan, core_recv) = unbounded();

        // Run the MainLoop
        let mainloop = {
            let mut core = Core::<U>::new(core_chan.clone());
            spawn(move || core.mainloop(core_recv))
        };

        // Open the buffers
        core_chan
            .send(Msg::CoreOpenBuffer("*scratch*".to_owned(), true))
            .unwrap();
        mainloop
    }

    fn mainloop(&mut self, recv: Receiver<Msg>) {
        loop {
            let msg = recv.recv().unwrap();
            let mut forward_view = None;
            match msg {
                Msg::Tick => {}
                Msg::Quit => {
                    self.quit();
                    break;
                }
                Msg::CoreOpenBuffer(ref urn, is_scratch) => {
                    self.open_buffer(urn, is_scratch);
                }
                Msg::MoveLeft(vid, _)
                | Msg::MoveRight(vid, _)
                | Msg::MoveUp(vid, _)
                | Msg::MoveDown(vid, _)
                | Msg::MoveToEOL(vid, _)
                | Msg::MoveToSOL(vid, _)
                | Msg::InsertAtCursor(vid, _)
                | Msg::InsertTextAtCursor(vid, _)
                | Msg::Backspace(vid)
                | Msg::ClientSize(vid, _, _)
                | Msg::SetOffset(vid, _)
                | Msg::SetCursorPos(vid, _, _)
                | Msg::CopySelectionDown(vid)
                | Msg::Delete(vid)
                | Msg::Atomic(vid, _) => forward_view = Some(vid),
                _ => (),
            }
            if let Some(vid) = forward_view {
                // eprintln!("{:?}", msg);
                self.views[vid].send(msg).unwrap();
            }
        }
    }

    fn open_buffer(&mut self, _urn: &str, _is_scratch: bool) {
        let (chan, handle) = View::run(
            BufferHandle::new(None),
            self.ui_chan.clone(),
            U::line_cache(),
        );
        self.views.push(chan.clone());
        chan.send(Msg::ShowView(self.views.len() - 1)).unwrap();
        chan.send(Msg::InsertTextAtCursor(0, U::scratch_text()))
            .unwrap();
        chan.send(Msg::SetCursorPos(0, 0, 0)).unwrap();
        self.view_handles.push(handle);
    }

    fn quit(&mut self) {
        for v in std::mem::take(&mut self.views) {
            v.send(Msg::Quit).ok();
        }
        for h in std::mem::take(&mut self.view_handles) {
            h.join().ok();
        }
        self.ui_chan.send(Msg::Quit).unwrap();
        self.ui_handle.take().unwrap().join().unwrap();
    }
}
