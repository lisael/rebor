use std::convert::TryFrom;
use std::io::stdin;
use std::thread::{spawn, JoinHandle};

use crossbeam_channel::{unbounded, Receiver, Sender};

use termion::event::{Event, Key as TKey};
use termion::input::TermRead;
use termion::{self, terminal_size};

use reborlib::core::UI;
use reborlib::input::{EditorMode, Input, InputHandler, Key};
use reborlib::message::Msg;
use reborlib::status::BufferStatus;
use reborlib::theming::{Chunk, ChunkContent, Face};
use reborlib::utils::num_digit;

use super::render::Render;

#[derive(Default)]
struct ClientStatus {
    edit_mode: EditorMode,
    current_command: String,
    cmd_pos: usize,
}

struct Status {
    buf_status: Option<(BufferStatus, Vec<u8>)>,
    old_buf_status: Option<(BufferStatus, Vec<u8>)>,
    client_status: ClientStatus,
}

impl Status {
    fn new() -> Self {
        Self {
            buf_status: None,
            old_buf_status: None,
            client_status: ClientStatus::default(),
        }
    }
}

pub struct TUI {
    line_offset: usize,
    lines: Vec<String>,
    term_size: (u16, u16),
    client_size: (u16, u16),
    input_chan: Sender<Input>,
    input_handle: Option<JoinHandle<()>>,
    status: Status,
    render: Render,
}

impl UI for TUI {
    fn run(core_chan: Sender<Msg>) -> (Sender<Msg>, JoinHandle<()>) {
        let (ui_chan, recv_chan) = unbounded();

        let mut ui = TUI::new(core_chan, ui_chan.clone());
        let ui_handler = spawn(move || ui.mainloop(recv_chan));
        (ui_chan, ui_handler)
    }
}

impl TUI {
    fn new(core_chan: Sender<Msg>, ui_chan: Sender<Msg>) -> Self {
        // Create the InputHandler
        let (input_chan, input_handle) = InputHandler::run(core_chan, ui_chan);
        Self {
            line_offset: 0,
            lines: vec![],
            term_size: (80, 24),
            client_size: (80, 23),
            input_chan,
            input_handle: Some(input_handle),
            status: Status::new(),
            render: Render::default(),
        }
    }

    fn read_input(&mut self) {
        let input_chan = self.input_chan.clone();
        spawn(move || {
            for c in stdin().events() {
                match c.unwrap() {
                    Event::Key(TKey::Char(c)) => input_chan.send(Input::Key(Key::Char(c))).unwrap(),
                    Event::Key(TKey::Left) => input_chan.send(Input::Key(Key::Left)).unwrap(),
                    Event::Key(TKey::Right) => input_chan.send(Input::Key(Key::Right)).unwrap(),
                    Event::Key(TKey::Up) => input_chan.send(Input::Key(Key::Up)).unwrap(),
                    Event::Key(TKey::Down) => input_chan.send(Input::Key(Key::Down)).unwrap(),
                    Event::Key(TKey::Backspace) => {
                        input_chan.send(Input::Key(Key::Backspace)).unwrap()
                    }
                    Event::Key(TKey::Esc) => input_chan.send(Input::Key(Key::Escape)).unwrap(),
                    _ => (),
                }
            }
        });
    }

    fn mainloop(&mut self, recv_chan: Receiver<Msg>) {
        // listen to stdin events
        self.read_input();
        // Init client size
        self.get_client_size();
        // listen to editor commands
        for order in recv_chan {
            match order {
                // Msg::UILines(lines) => self.set_lines(lines),
                Msg::BufferStatus(bs) => {
                    self.status.old_buf_status = self.status.buf_status.take();
                    self.status.buf_status = Some((bs, vec![]));
                    self.redraw();
                }
                Msg::Quit => {
                    self.input_chan.send(Input::Quit).unwrap();
                    if let Some(h) = self.input_handle.take() {
                        h.join().unwrap()
                    }
                    break;
                }
                Msg::SetEditorMode(mode) => {
                    let mut cs = &mut self.status.client_status;
                    match mode {
                        EditorMode::Normal | EditorMode::Goto => {
                            self.render.set_cursor_face(Face::SelectCursor)
                        }
                        EditorMode::Insert | EditorMode::Command => {
                            self.render.set_cursor_face(Face::InsertCursor)
                        }
                    }
                    cs.edit_mode = mode;
                    cs.current_command = "".to_owned();
                    cs.cmd_pos = 0;
                    self.redraw();
                }
                Msg::SetEditorCommand(cmd, pos) => {
                    let mut cs = &mut self.status.client_status;
                    cs.current_command = cmd;
                    cs.cmd_pos = pos;
                    self.redraw();
                }
                // UIMsg::ViewStatus(status) => ui.set_status(status),
                _ => (),
            }
        }
    }

    fn redraw(&mut self) {
        let bs = self.status.buf_status.clone();
        let mut max_idx = 0;
        let mut padding = 2;
        match bs {
            None => (),
            Some(bs) => {
                let offset = bs.0.client_offset;
                max_idx = bs.0.content.len();
                padding = num_digit(bs.0.buf_line_length) + 1;
                for (idx, line) in bs.0.content.iter().enumerate() {
                    // let line = Chunk {
                    //     face: Face::Default,
                    //     content: ChunkContent::Str(line.clone()),
                    // };
                    let lnum = {
                        let raw = format!("{}│", offset + idx + 1);
                        let len = raw.chars().count();
                        if padding >= len {
                            let mut padded = String::with_capacity(padding);
                            for _ in 0..(padding - len) {
                                padded.push(' ');
                            }
                            padded.push_str(&raw);
                            padded.to_owned()
                        } else {
                            raw
                        }
                    };
                    let line = Chunk {
                        start: 0,
                        end: lnum.len(),
                        face: Face::Default,
                        content: ChunkContent::Chunks(vec![
                            Chunk {
                                start: 0,
                                end: lnum.len(),
                                face: Face::LineNumber,
                                content: ChunkContent::Str(lnum),
                            },
                            // Chunk {
                            //     face: Face::LineNumber,
                            //     content: ChunkContent::Str("--".to_owned()),
                            // },
                        ]),
                    }
                    .push(line.clone());
                    let line_nr = u16::try_from(idx).unwrap();
                    self.draw_line(line_nr, line);
                }
            }
        };
        if max_idx < (self.term_size.1 as usize - 1) {
            let filler = {
                let raw = "~│";
                let mut padded = String::with_capacity(padding);
                for _ in 0..(padding - 2) {
                    padded.push(' ');
                }
                padded.push_str(&raw);
                padded
            };
            for i in max_idx..(self.term_size.1 as usize - 1) {
                self.draw_line(
                    i as u16,
                    Chunk {
                        start: 0,
                        end: padding,
                        face: Face::LineNumber,
                        content: ChunkContent::Str(filler.clone()),
                    },
                )
            }
        }
        self.draw_status();
        self.render.draw();
    }

    /// recursively draw chunks in the renderer, returning the last filled column
    fn draw_chunk(&mut self, line: u16, col: u16, chunk: Chunk) -> u16 {
        let mut col = col;
        match chunk.content {
            ChunkContent::Empty => {
                if chunk.face == Face::SelectCursor {
                    self.render.set_cursor_pos(col, line)
                }
                col
            }
            ChunkContent::Str(s) => {
                for c in s.chars() {
                    if c == '\n' {
                        break;
                    }
                    self.render.put((col as u16, line), c, &chunk.face);
                    col += 1;
                }
                col
            }
            ChunkContent::Chunks(chunks) => {
                for c in chunks {
                    col = self.draw_chunk(line, col, c);
                }
                col
            }
            _ => unreachable!(),
        }
    }

    fn draw_line(&mut self, line_nr: u16, line: Chunk) {
        self.draw_chunk(line_nr, 0, line);
    }

    fn get_client_size(&mut self) {
        self.term_size = terminal_size().unwrap_or((80, 24));
        // update the render
        self.render.set_term_size(self.term_size);
        // compute the buffer rectangle size and send the result to core.
        let (mut w, mut h) = self.term_size;
        // status line
        h -= 1;
        // line numbers
        w -= match &self.status.buf_status {
            None => 2,
            Some(bs) => num_digit(bs.0.buf_line_length) as u16 + 1,
        };
        self.input_chan.send(Input::ClientSize(w, h)).unwrap();
    }

    fn draw_status(&mut self) {
        let mut chunks = vec![];
        let cs = &self.status.client_status;
        chunks.push(match cs.edit_mode {
            EditorMode::Insert => Chunk {
                start: 0,
                end: 0,
                face: Face::Default,
                content: ChunkContent::Str("INSERT".to_owned()),
            },
            EditorMode::Normal => Chunk {
                start: 0,
                end: 0,
                face: Face::Default,
                content: ChunkContent::Str("NORMAL".to_owned()),
            },
            EditorMode::Goto => Chunk {
                start: 0,
                end: 0,
                face: Face::Default,
                content: ChunkContent::Str("GOTO".to_owned()),
            },
            EditorMode::Command => Chunk {
                start: 0,
                end: 0,
                face: Face::Default,
                content: ChunkContent::Str(":".to_owned() + &cs.current_command),
            },
        });
        if let Some(bs) = self.status.buf_status.as_mut() {
            chunks.push(Chunk {
                start: 0,
                end: 0,
                face: Face::Default,
                content: ChunkContent::Str(format!(
                    " {} {}:{} / {}",
                    bs.0.buf_name,
                    bs.0.cursor_line + 1,
                    bs.0.cursor_col + 1,
                    bs.0.buf_line_length,
                )),
            })
        }
        if let EditorMode::Command = cs.edit_mode {
            self.render
                .set_cursor_pos((cs.cmd_pos + 1) as u16, self.term_size.1 - 1)
        }
        self.draw_line(
            self.term_size.1 - 1,
            Chunk {
                start: 0,
                end: 0,
                face: Face::Default,
                content: ChunkContent::Chunks(chunks),
            },
        );
    }
}
