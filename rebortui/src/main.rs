use reborlib::core::Core;
mod render;
mod ui;

use ui::TUI;

fn main() {
    match Core::<TUI>::run().join() {
        Ok(_) => (),
        Err(e) => eprintln!("{:#?}", e),
    };
}
