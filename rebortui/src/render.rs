use std::collections::HashMap;
use std::io::{self, stdout, Stdout, Write};

use termion::color;
use termion::raw::{IntoRawMode, RawTerminal};
use termion::screen::AlternateScreen;
use termion::style;

use reborlib::theming::{Face, Style, StyleMap as StyleMap_, StyleModifier};

type StyleMap = StyleMap_<u8>;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct Char {
    ch: char,
    style: Style<u8>,
}

impl Default for Char {
    fn default() -> Self {
        Char {
            ch: ' ',
            style: Style::default(),
        }
    }
}

#[derive(Default)]
struct StyleCache {
    buf: Vec<u8>,
    cur_fg: Option<u8>,
    cur_bg: Option<u8>,
    cur_style: Option<StyleModifier>,
}

impl StyleCache {
    fn into_vec(self) -> Vec<u8> {
        self.buf
    }

    fn reset_fg(&mut self) -> io::Result<()> {
        if self.cur_fg.is_some() {
            self.cur_fg = None;
            write!(&mut self.buf, "{}", color::Fg(color::Reset),)?;
        }
        Ok(())
    }

    fn reset_bg(&mut self) -> io::Result<()> {
        if self.cur_bg.is_some() {
            self.cur_bg = None;
            write!(&mut self.buf, "{}", color::Bg(color::Reset),)?;
        }
        Ok(())
    }

    fn reset_style(&mut self) -> io::Result<()> {
        if self.cur_style.is_some() {
            self.cur_style = None;
            write!(&mut self.buf, "{}", style::Reset)?;
        }
        Ok(())
    }

    fn reset_all(&mut self) -> io::Result<()> {
        self.reset_fg()?;
        self.reset_bg()?;
        self.reset_style()?;

        Ok(())
    }

    fn change_fg(&mut self, fg: color::AnsiValue) -> io::Result<()> {
        if self.cur_fg != Some(fg.0) {
            self.cur_fg = Some(fg.0);
            write!(&mut self.buf, "{}", color::Fg(fg),)?;
        }
        Ok(())
    }

    fn change_bg(&mut self, bg: color::AnsiValue) -> io::Result<()> {
        if self.cur_bg != Some(bg.0) {
            self.cur_bg = Some(bg.0);
            write!(&mut self.buf, "{}", color::Bg(bg),)?;
        }
        Ok(())
    }

    fn change_style(&mut self, modifier: StyleModifier) -> io::Result<()> {
        if self.cur_style != Some(modifier) {
            self.cur_style = Some(modifier);
            use StyleModifier::*;
            match modifier {
                Bold => write!(&mut self.buf, "{}", style::Bold)?,
                _ => write!(&mut self.buf, "{}", style::Reset)?,
            };
        }
        Ok(())
    }

    fn set_style(&mut self, style: Style<u8>) -> io::Result<()> {
        if let Some(fg) = style.fg {
            self.change_fg(color::AnsiValue(fg))?;
        } else {
            self.reset_fg()?;
        }
        if let Some(bg) = style.bg {
            self.change_bg(color::AnsiValue(bg))?;
        } else {
            self.reset_bg()?;
        }

        if let Some(style) = style.style {
            self.change_style(style)?;
        } else {
            self.reset_style()?;
        }

        Ok(())
    }
}

impl io::Write for StyleCache {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        self.buf.write(buf)
    }

    fn flush(&mut self) -> io::Result<()> {
        self.buf.flush()
    }
}

pub struct Render {
    screen: AlternateScreen<RawTerminal<Stdout>>,
    display_cols: usize,
    display_rows: usize,
    oldbuf: Vec<Char>,
    curbuf: Vec<Char>,
    theme: StyleMap,
    cursor_pos: (u16, u16),
    cursor_face: Face,
}

impl Render {
    pub fn set_term_size(&mut self, term_size: (u16, u16)) {
        let cols = term_size.0 as usize;
        let rows = term_size.1 as usize;
        if cols != self.display_cols || rows != self.display_rows {
            self.display_cols = cols;
            self.display_rows = rows;
            self.oldbuf.truncate(0);
            self.curbuf.truncate(0);
            self.curbuf.resize_with(cols * rows, Default::default);
        }
    }

    pub fn put(&mut self, coord: (u16, u16), ch: char, face: &Face) {
        let style = self.theme.get(face);
        if let Some(c) = self.char_at_mut((coord.0 as usize, coord.1 as usize)) {
            *c = Char { ch, style }
        }
    }

    fn coord_to_i(&self, coord: (usize, usize)) -> usize {
        coord.0 + coord.1 * self.display_cols
    }

    fn char_at_mut(&mut self, coord: (usize, usize)) -> Option<&mut Char> {
        let i = self.coord_to_i(coord);
        self.curbuf.get_mut(i)
    }

    pub fn draw(&mut self) {
        let mut buf = StyleCache::default();
        if self.oldbuf.is_empty() {
            self.oldbuf.resize_with(self.curbuf.len(), Char::default);
            write!(&mut buf, "{}{}", style::Reset, termion::clear::All).unwrap();
            buf.reset_all().unwrap();
        }
        self.draw_diff(&mut buf);
        self.draw_cursor(&mut buf);

        buf.reset_all().unwrap();

        let buf = buf.into_vec();
        buf.len();

        self.screen.write_all(&buf).unwrap();
        self.screen.flush().unwrap();
        std::mem::swap(&mut self.oldbuf, &mut self.curbuf);
        self.curbuf.truncate(0);
        self.curbuf.resize_with(self.oldbuf.len(), Default::default);
    }

    pub fn set_cursor_pos(&mut self, col: u16, line: u16) {
        self.cursor_pos = (col, line);
    }

    pub fn set_cursor_face(&mut self, face: Face) {
        self.cursor_face = face;
    }

    fn draw_cursor(&mut self, buf: &mut StyleCache) {
        if self.cursor_face == Face::HiddenCursor {
            write!(buf, "{}", termion::cursor::Hide).unwrap();
        } else {
            match self.cursor_face {
                Face::InsertCursor => write!(buf, "\x1b[5 q",).unwrap(),
                Face::SelectCursor => write!(buf, "\x1b[2 q",).unwrap(),
                _ => panic!("bad cursor face"),
            }
            write!(
                buf,
                "{}{}",
                termion::cursor::Goto(self.cursor_pos.0 + 1, self.cursor_pos.1 + 1),
                termion::cursor::Show,
            )
            .unwrap();
        }
    }

    fn draw_diff(&mut self, buf: &mut StyleCache) {
        let mut needs_goto = true;

        for (ch_i, new_ch) in self.curbuf.iter().copied().enumerate() {
            let old_ch = self.oldbuf[ch_i];

            if new_ch == old_ch {
                needs_goto = true;
                continue;
            }

            if needs_goto {
                needs_goto = false;

                let term_line = ch_i / self.display_cols + 1;
                let term_column = ch_i % self.display_cols + 1;

                write!(
                    buf,
                    "{}",
                    termion::cursor::Goto(term_column as u16, term_line as u16)
                )
                .unwrap();
            }

            buf.set_style(new_ch.style).unwrap();

            write!(buf, "{}", new_ch.ch).unwrap();
        }
    }
}

fn default_styles() -> StyleMap {
    let mut map = HashMap::with_capacity(3);
    map.insert(Face::Default, Style::<u8>::default());
    map.insert(
        Face::LineNumber,
        Style {
            fg: Some(10),
            ..Default::default()
        },
    );
    map.insert(
        Face::Whitespace,
        Style {
            fg: Some(10),
            ..Default::default()
        },
    );
    StyleMap::from_map(map)
}

impl Default for Render {
    fn default() -> Self {
        Self {
            screen: AlternateScreen::from(stdout().into_raw_mode().unwrap()),
            display_cols: 80,
            display_rows: 24,
            oldbuf: Vec::with_capacity(80 * 24),
            curbuf: Vec::with_capacity(80 * 24),
            theme: default_styles(),
            cursor_pos: (0, 0),
            cursor_face: Face::HiddenCursor,
        }
    }
}
