use reborlib::core::Core;
mod buffer_widget;
mod ui;

use ui::GUI;

fn main() {
    match Core::<GUI>::run().join() {
        Ok(_) => (),
        Err(e) => eprintln!("{:#?}", e),
    };
}
