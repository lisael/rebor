use std::cmp::min;
use std::collections::HashMap;

use crossbeam_channel::Sender;
use im::Vector;

use druid::widget::prelude::*;
use druid::widget::Axis;
use druid::widget::Scroll;
use druid::{
    kurbo::Line,
    piet::{
        Color, FontFamily, FontWeight, PietText, PietTextLayout, Text, TextAttribute, TextLayout,
        TextLayoutBuilder,
    },
    Data, Env, PaintCtx, Point, RenderContext, Selector, Vec2, WidgetExt, WidgetPod,
};

use reborlib::core::UI;
use reborlib::input::{EditorMode, Input, InputHandler, Key};
use reborlib::theming::{Chunk, ChunkContent, Face, Style as Style_, StyleMap as StyleMap_};
use reborlib::utils::num_digit;

use crate::ui::{BufferState, GUI};

type StyleMap = StyleMap_<Color>;
type Style = Style_<Color>;

const EDITOR_FIX_SCROLL: Selector = Selector::new("reborgui.editor_layout_changed");

const BACKGROUND: Color = Color::rgb8(0x28, 0x28, 0x28);
const FORGROUND: Color = Color::rgb8(0xc5, 0xc8, 0xc6);
const FONT: FontFamily = FontFamily::MONOSPACE;
const FONT_SIZE: f64 = 14.0;
const WEIGHT: FontWeight = FontWeight::MEDIUM;
const CURSER_WIDTH: f64 = 2.;
const CURSER_COLOR: Color = Color::rgb8(0xfe, 0xfd, 0xbf);

fn get_char_rect(piet_text: &mut PietText) -> Vec2 {
    let layout = get_layout(piet_text, "1234\n67");
    let top = layout.hit_test_text_position(1).point.y;
    let bottom = layout.hit_test_text_position(6).point.y;
    let left = layout.hit_test_text_position(2).point.x;
    let right = layout.hit_test_text_position(3).point.x;
    Vec2::new(right - left, bottom - top)
}

fn get_layout(piet_text: &mut PietText, text: &str) -> PietTextLayout {
    piet_text
        .new_text_layout(text.to_string())
        .font(FONT, FONT_SIZE)
        .default_attribute(TextAttribute::TextColor(FORGROUND))
        .default_attribute(TextAttribute::Weight(WEIGHT))
        .build()
        .unwrap()
}

#[derive(Clone)]
struct ScrollState {
    char_rect: Vec2,
    display_size: Vec2,
    max_line_length: usize,
    line_length: usize,
    client_offset: usize,
    content: Vector<Chunk>,
    mode: EditorMode,
}

impl Data for ScrollState {
    fn same(&self, other: &Self) -> bool {
        self.line_length.same(&other.line_length)
            & self.char_rect.same(&other.char_rect)
            & self.display_size.same(&other.display_size)
            & self.max_line_length.same(&other.max_line_length)
            & self.line_length.same(&other.line_length)
            & self.client_offset.same(&other.client_offset)
            & (self.mode == other.mode)
            & (if self.content.is_inline() {
                self.content.len() == other.content.len()
                    && self
                        .content
                        .iter()
                        .zip(other.content.iter())
                        .all(|(a, b)| a == b)
            } else {
                self.content.ptr_eq(&other.content)
            })
    }
}

#[derive(Clone, Data)]
struct LineNrState {
    vscroll: f64,
    char_rect: Vec2,
    display_lines: u16,
    line_length: usize,
    display_size: Vec2,
    client_offset: usize,
}

struct LineNr {}

impl Widget<LineNrState> for LineNr {
    fn event(
        &mut self,
        _ctx: &mut EventCtx<'_, '_>,
        _event: &Event,
        _data: &mut LineNrState,
        _env: &Env,
    ) {
    }

    fn lifecycle(
        &mut self,
        _ctx: &mut LifeCycleCtx<'_, '_>,
        _event: &LifeCycle,
        _data: &LineNrState,
        _env: &Env,
    ) {
    }

    fn update(
        &mut self,
        ctx: &mut UpdateCtx<'_, '_>,
        _old_data: &LineNrState,
        _data: &LineNrState,
        _env: &Env,
    ) {
        ctx.request_layout();
        // ctx.request_paint();
    }

    fn layout(
        &mut self,
        _ctx: &mut LayoutCtx<'_, '_>,
        _bc: &BoxConstraints,
        data: &LineNrState,
        _env: &Env,
    ) -> Size {
        Size {
            width: data.char_rect.x * num_digit(data.line_length) as f64 + 4.,
            height: data.display_size.y,
        }
    }

    fn paint(&mut self, ctx: &mut PaintCtx, data: &LineNrState, _env: &Env) {
        ctx.with_save(|rc| {
            let mut text_pos = {
                let y = (data.client_offset as f64 * data.char_rect.y) - data.vscroll;
                Point::new(0., y)
            };
            let start = data.client_offset;
            let end = usize::min(start + data.display_lines as usize, data.line_length);
            let padding = num_digit(data.line_length);
            for nr in start..end {
                let lnum = {
                    let raw = format!("{}", nr + 1);
                    let len = raw.chars().count();
                    if padding >= len {
                        let mut padded = String::with_capacity(padding);
                        for _ in 0..(padding - len) {
                            padded.push(' ');
                        }
                        padded.push_str(&raw);
                        padded.to_owned()
                    } else {
                        raw
                    }
                };
                let text_layout = get_layout(&mut rc.text(), &lnum);
                rc.draw_text(&text_layout, text_pos);
                text_pos.y += data.char_rect.y;
            }
            let size = rc.size();
            let top = Point::new(size.width - 2., 0.);
            let bottom = Point::new(size.width - 2., size.height);
            let line = Line::new(top, bottom);
            rc.stroke(line, &CURSER_COLOR, 1.);
        });
    }
}

pub(crate) struct ScrollEditWidget {
    scroll: WidgetPod<ScrollState, Scroll<ScrollState, EditWidget>>,
    yscroll: Option<f64>,
    linenr: WidgetPod<LineNrState, LineNr>,
    input_chan: Sender<Input>,
    view_size: (u16, u16),
    char_rect: Vec2,
    display_size: Vec2,
    forced_offset: usize,
}

impl ScrollEditWidget {
    pub fn new(input_chan: Sender<Input>) -> Self {
        Self {
            scroll: WidgetPod::new(Scroll::new(EditWidget::new(input_chan.clone()))),
            linenr: WidgetPod::new(LineNr {}),
            yscroll: None,
            input_chan,
            view_size: (0, 0),
            char_rect: Vec2::default(),
            display_size: Vec2::default(),
            forced_offset: usize::MAX,
        }
    }

    fn linenr_data(&self, bs: &BufferState) -> LineNrState {
        LineNrState {
            vscroll: self.scroll.widget().offset_for_axis(Axis::Vertical),
            char_rect: self.char_rect,
            display_lines: self.view_size.1,
            line_length: bs.line_length,
            display_size: self.display_size,
            client_offset: bs.client_offset,
        }
    }

    fn scroll_data(&self, bs: &BufferState) -> ScrollState {
        ScrollState {
            char_rect: self.char_rect,
            display_size: self.display_size,
            max_line_length: bs.max_line_length,
            line_length: bs.line_length,
            client_offset: bs.client_offset,
            content: bs.content.clone(),
            mode: bs.edit_mode.clone(),
        }
    }
}

impl Widget<BufferState> for ScrollEditWidget {
    fn event(&mut self, ctx: &mut EventCtx, event: &Event, data: &mut BufferState, env: &Env) {
        match event {
            Event::Command(cmd) if cmd.is(EDITOR_FIX_SCROLL) => {
                ctx.request_update();
                ctx.set_handled();
            }
            _ => (),
        };
        let before = self.scroll.widget().offset();
        self.scroll
            .event(ctx, event, &mut self.scroll_data(data), env);
        let after = self.scroll.widget().offset();
        if before != after {
            self.forced_offset = (after.y / self.char_rect.y) as usize;
            self.input_chan
                .send(Input::SetOffset(self.forced_offset))
                .unwrap();
            ctx.request_update();
        }
    }

    fn lifecycle(
        &mut self,
        ctx: &mut LifeCycleCtx,
        event: &LifeCycle,
        data: &BufferState,
        env: &Env,
    ) {
        if let LifeCycle::WidgetAdded = event {
            // compute the view size (cols x lines)
            self.char_rect = get_char_rect(ctx.text());
        }
        self.linenr
            .lifecycle(ctx, event, &self.linenr_data(data), env);
        self.scroll
            .lifecycle(ctx, event, &self.scroll_data(data), env);
    }

    fn update(
        &mut self,
        ctx: &mut UpdateCtx,
        old_data: &BufferState,
        data: &BufferState,
        env: &Env,
    ) {
        if ((old_data.client_offset != data.client_offset)
            & (data.client_offset != self.forced_offset))
            | (self.yscroll.is_some())
        {
            let yscroll = (data.client_offset) as f64 * self.char_rect.y;
            self.scroll
                .widget_mut()
                .scroll_to_on_axis(Axis::Vertical, yscroll);

            // the editor widget layout is computed after we request a scroll.
            // If the Editor widget is not high enough (e.g. a lot of lines are going
            // to be added) the scroll widget can't honor our request, resulting in a ugly
            // glitch. The fix is to retry in the next event cycle.
            if self.scroll.widget().offset_for_axis(Axis::Vertical) != yscroll {
                self.yscroll = Some(yscroll);
                ctx.submit_command(EDITOR_FIX_SCROLL.to(ctx.widget_id()));
            } else {
                self.yscroll = None;
            };
        }
        self.scroll.update(ctx, &self.scroll_data(data), env);
        self.linenr.update(ctx, &self.linenr_data(data), env);
    }

    fn layout(
        &mut self,
        ctx: &mut LayoutCtx,
        bc: &BoxConstraints,
        data: &BufferState,
        env: &Env,
    ) -> Size {
        let mut width = bc.max().width;
        let height = bc.max().height;

        // compute the line x columns size
        let max_lines_onscreen = (height / self.char_rect.y).ceil() as u16;
        let max_columns = (width / self.char_rect.x).floor() as u16;
        let new_size = (max_columns, max_lines_onscreen);
        // If it changed, inform the editor core so it may update the number of lines sent to us.
        if self.view_size != new_size {
            self.input_chan
                .send(Input::ClientSize(new_size.0, new_size.1))
                .unwrap();
            self.view_size = new_size;
        }

        // layout the line numbers
        self.display_size = Vec2::new(width, height);
        let mut origin = Point::ORIGIN;
        let nr_data = &self.linenr_data(data);
        let nr_size = self.linenr.layout(ctx, bc, nr_data, env);
        self.linenr.set_origin(ctx, nr_data, env, origin);

        // layout the editor
        origin += Vec2::new(nr_size.width, 0.);
        width -= nr_size.width;
        self.display_size = Vec2::new(width, height);
        // we must check if the scroll offset changed while updating the scroll widget and inform the view
        // about the new line offset.
        // TODO: use a macro for this as it's copypasta from self.event()
        let before = self.scroll.widget().offset_for_axis(Axis::Vertical);
        self.scroll.layout(
            ctx,
            &BoxConstraints::tight(Size { width, height }),
            &self.scroll_data(data),
            env,
        );
        self.scroll
            .set_origin(ctx, &self.scroll_data(data), env, origin);
        let after = self.scroll.widget().offset_for_axis(Axis::Vertical);
        if before != after {
            self.forced_offset = (after / self.char_rect.y) as usize;
            self.input_chan
                .send(Input::SetOffset(self.forced_offset))
                .unwrap();
        }
        bc.max()
    }

    fn paint(&mut self, ctx: &mut PaintCtx, data: &BufferState, env: &Env) {
        self.scroll.paint(ctx, &self.scroll_data(data), env);
        self.linenr.paint(ctx, &self.linenr_data(data), env);
    }
}

pub(crate) struct EditWidget {
    input_chan: Sender<Input>,
    styles: StyleMap,
    edit_mode: bool,
    old_lines: Vector<Chunk>,
}

fn default_styles() -> StyleMap {
    let mut map = HashMap::with_capacity(3);
    map.insert(
        Face::Default,
        Style {
            bg: Some(Color::rgb8(0x28, 0x28, 0x28)),
            fg: Some(Color::rgb8(0xc5, 0xc8, 0xc6)),
            style: None,
        },
    );
    map.insert(
        Face::MainSelectionBody,
        Style {
            bg: Some(Color::rgb8(0x50, 0x30, 0x40)),
            fg: None,
            style: None,
        },
    );
    map.insert(
        Face::MainSelectionCursor,
        Style {
            bg: Some(Color::rgb8(0x50, 0x30, 0x40)),
            fg: None,
            style: None,
        },
    );
    map.insert(
        Face::SecondarySelectionBody,
        Style {
            bg: Some(Color::rgb8(0x42, 0x40, 0x55)),
            fg: None,
            style: None,
        },
    );
    map.insert(
        Face::SecondarySelectionCursor,
        Style {
            bg: Some(Color::rgb8(0x42, 0x40, 0x55)),
            fg: None,
            style: None,
        },
    );
    StyleMap::from_map(map)
}

impl EditWidget {
    pub fn new(input_chan: Sender<Input>) -> Self {
        Self {
            input_chan,
            styles: default_styles(),
            edit_mode: false,
            old_lines: Default::default(),
        }
    }

    fn paint_line(&self, ctx: &mut PaintCtx, line: &Chunk, text_pos: &mut Point, char_rect: &Vec2) {
        // dbg!(&line);
        self.paint_chunk(ctx, line, text_pos, char_rect);
        text_pos.x = 0.;
        text_pos.y += char_rect.y;
    }

    fn paint_line_cursor(
        &self,
        ctx: &mut PaintCtx,
        text_pos: &Point,
        char_rect: &Vec2,
        after: bool,
    ) {
        let xshift = if after { char_rect.x } else { 0. };
        let top = Point::new(text_pos.x + xshift, text_pos.y);
        let bottom = Point::new(text_pos.x + xshift, text_pos.y + char_rect.y * 0.8);
        let line = Line::new(top, bottom);
        ctx.stroke(line, &CURSER_COLOR, CURSER_WIDTH);
    }

    fn paint_text(
        &self,
        ctx: &mut PaintCtx,
        text_pos: &mut Point,
        text: &str,
        style: &Style,
        char_rect: &Vec2,
    ) {
        let mut style = self.styles.get(&Face::Default).paintover(style);
        let bg = style
            .bg
            .take()
            .expect("The background color must be defined for the default face");
        let fg = style
            .fg
            .take()
            .expect("The forground color must be defined for the default face");
        let layout = ctx
            .text()
            .new_text_layout(text.to_string())
            .font(FONT, FONT_SIZE)
            .default_attribute(TextAttribute::TextColor(fg))
            .default_attribute(TextAttribute::Weight(WEIGHT))
            .build()
            .unwrap();
        let rect = layout.image_bounds().with_origin(*text_pos);
        let height = char_rect.y * 0.8;
        let width = {
            let rectw = layout.trailing_whitespace_width() - char_rect.x * 0.1;
            let minw = char_rect.x * 0.9;
            minw.max(rectw)
        };
        let rect = rect.with_size(Size { width, height });

        ctx.fill(rect, &bg);
        ctx.draw_text(&layout, *text_pos);
        text_pos.x += layout.trailing_whitespace_width();
    }

    fn paint_chunk(
        &self,
        ctx: &mut PaintCtx,
        chunk: &Chunk,
        text_pos: &mut Point,
        char_rect: &Vec2,
    ) {
        let line_height = char_rect.y;
        match &chunk.content {
            ChunkContent::Nop => match &chunk.face {
                Face::MainSelectionCollapsed | Face::SecondarySelectionCollapsed => {
                    if self.edit_mode {
                        self.paint_line_cursor(ctx, text_pos, char_rect, false)
                    } else {
                        self.paint_text(
                            ctx,
                            text_pos,
                            " ",
                            &self.styles.get(&Face::Default).invert(),
                            char_rect,
                        )
                    }
                }
                _ => (),
            },
            ChunkContent::Str(s) => {
                let mut has_cursor = false;
                let mut shift_cursor = false;
                let mut style = self.styles.get(&Face::Default);
                match &chunk.face {
                    Face::Composed(faces) => {
                        let nul_style = Style {
                            fg: None,
                            bg: None,
                            style: None,
                        };
                        for f in faces {
                            style = style.paintover(&self.styles.get_or(f, &nul_style));
                            match f {
                                &Face::MainSelectionCollapsed
                                | &Face::SecondarySelectionCollapsed => {
                                    has_cursor = true;
                                }
                                &Face::MainSelectionCursor | Face::SecondarySelectionCursor => {
                                    has_cursor = true;
                                    shift_cursor = true;
                                }
                                _ => (),
                            }
                        }
                    }
                    Face::MainSelectionCollapsed | Face::SecondarySelectionCollapsed => {
                        has_cursor = true;
                    }
                    _ => (),
                }
                if has_cursor & !self.edit_mode {
                    style = self.styles.get(&Face::Default).invert();
                }
                let orig_pos = text_pos.clone();
                self.paint_text(ctx, text_pos, s, &style, char_rect);
                if has_cursor & self.edit_mode {
                    self.paint_line_cursor(ctx, &orig_pos, char_rect, shift_cursor);
                }
            }
            ChunkContent::Chunks(chunks) => {
                for c in chunks {
                    self.paint_chunk(ctx, c, text_pos, char_rect);
                }
            }
            ChunkContent::Empty => {
                if chunk.face == Face::SelectCursor {
                    let top = Point::new(text_pos.x, text_pos.y);
                    let bottom = Point::new(text_pos.x, text_pos.y + line_height * 0.8);
                    let line = Line::new(top, bottom);
                    ctx.stroke(line, &CURSER_COLOR, CURSER_WIDTH);
                }
            }
            _ => (),
        }
    }

    fn input_send(&mut self, input: Input) {
        self.input_chan.send(input).unwrap();
    }

    fn get_buffer_coords_at(
        &self,
        ctx: &mut EventCtx,
        x: f64,
        y: f64,
        data: &ScrollState,
    ) -> (usize, usize) {
        let line = (y / data.char_rect.y).floor() as usize;
        let content_line = line - data.client_offset.saturating_sub(GUI::line_cache());
        let column = {
            if let Some(l) = data.content.get(content_line) {
                self.get_line_column_at(ctx, x, &l)
            } else {
                0
            }
        };
        (line, column)
    }

    fn get_line_column_at(&self, ctx: &mut EventCtx, x: f64, line: &Chunk) -> usize {
        let mut result = 0;
        let mut acc = 0.;
        let piet = ctx.text();
        for (idx, c) in line.chars().enumerate() {
            let mut buf = [0; 4];
            acc += get_layout(piet, c.encode_utf8(&mut buf)).trailing_whitespace_width();
            if acc >= x {
                return idx;
            }
            result = idx;
        }
        result
    }
}

impl Widget<ScrollState> for EditWidget {
    fn event(
        &mut self,
        ctx: &mut EventCtx<'_, '_>,
        event: &Event,
        data: &mut ScrollState,
        _env: &Env,
    ) {
        // TODO: real focus management
        ctx.request_focus();

        match event {
            Event::KeyDown(key_event) => {
                use druid::keyboard_types::Key::*;
                // dbg!(&key_event);
                match &key_event.key {
                    ArrowLeft => self.input_send(Input::Key(Key::Left)),
                    ArrowRight => self.input_send(Input::Key(Key::Right)),
                    ArrowUp => self.input_send(Input::Key(Key::Up)),
                    ArrowDown => self.input_send(Input::Key(Key::Down)),
                    Backspace => self.input_send(Input::Key(Key::Backspace)),
                    Escape => self.input_send(Input::Key(Key::Escape)),
                    Enter => self.input_send(Input::Key(Key::Char('\n'))),
                    Tab => self.input_send(Input::Key(Key::Char('\t'))),
                    Character(chars) => {
                        if data.mode == EditorMode::Insert {
                            self.input_send(Input::Text(chars.to_owned()));
                        } else {
                            for c in chars.chars() {
                                self.input_send(Input::Key(Key::Char(c)));
                            }
                        }
                    }

                    _ => {}
                }
            }
            Event::MouseUp(m) => {
                match self.get_buffer_coords_at(ctx, m.pos.x, m.pos.y, data) {
                    (l, c) => self.input_send(Input::SetCursorPos(l, c)),
                };
            }
            _ => {}
        }
    }

    fn lifecycle(
        &mut self,
        ctx: &mut LifeCycleCtx<'_, '_>,
        event: &LifeCycle,
        _data: &ScrollState,
        _env: &Env,
    ) {
        if let LifeCycle::WidgetAdded = event {
            ctx.register_for_focus();
        }
    }

    fn update(
        &mut self,
        ctx: &mut UpdateCtx<'_, '_>,
        old_data: &ScrollState,
        data: &ScrollState,
        _env: &Env,
    ) {
        self.old_lines = old_data.content.clone();
        self.edit_mode = data.mode == EditorMode::Insert;
        ctx.request_layout();
    }

    fn layout(
        &mut self,
        _ctx: &mut LayoutCtx<'_, '_>,
        _bc: &BoxConstraints,
        data: &ScrollState,
        _env: &Env,
    ) -> Size {
        let mut width = data.max_line_length as f64 * data.char_rect.x;
        if width < data.display_size.x {
            width = data.display_size.x;
        }
        let mut height = (data.line_length as f64 * data.char_rect.y) + (data.display_size.y / 2.);
        if height < data.display_size.y {
            height = data.display_size.y;
        }
        Size { width, height }
    }

    fn paint(&mut self, ctx: &mut PaintCtx<'_, '_, '_>, data: &ScrollState, _env: &Env) {
        let clip_rect = ctx.size().to_rect();
        ctx.fill(clip_rect, &BACKGROUND);

        ctx.with_save(|rc| {
            let mut text_pos = Point::new(
                0.0,
                data.client_offset.saturating_sub(GUI::line_cache()) as f64 * data.char_rect.y,
            );
            rc.clip(clip_rect);
            for line in &data.content {
                self.paint_line(rc, line, &mut text_pos, &data.char_rect);
            }
        });
    }
}
