use std::any::Any;
use std::sync::Arc;
use std::thread::{spawn, JoinHandle};

use im::{vector, Vector};

use crossbeam_channel::{unbounded, Receiver, Sender};

use reborlib::core::UI;
use reborlib::input::{EditorMode, Input, InputHandler, Key};
use reborlib::message::Msg;
use reborlib::status::BufferStatus;
use reborlib::theming::Chunk;

use druid::commands::QUIT_APP;
use druid::widget::{Button, Flex, Label};
use druid::{
    AppDelegate, AppLauncher, Application, ArcStr, Command, Data, DelegateCtx, Env, ExtEventSink,
    FontDescriptor, FontFamily, FontWeight, Handled, Lens, PlatformError, Selector, Target, Widget,
    WidgetExt, WindowDesc,
};

use crate::buffer_widget::ScrollEditWidget;

const FONT: FontFamily = FontFamily::MONOSPACE;
const FONT_SIZE: f64 = 14.0;
const WEIGHT: FontWeight = FontWeight::MEDIUM;

#[derive(Default, Clone, PartialEq, Eq, Lens)]
struct ClientStatus {
    edit_mode: EditorMode,
    current_command: String,
    cmd_pos: usize,
}

impl Data for ClientStatus {
    fn same(&self, other: &Self) -> bool {
        self == other
    }
}

#[derive(Clone)]
pub(crate) struct BufferState {
    pub name: ArcStr,
    pub line_length: usize,
    pub client_offset: usize,
    pub cursor_line: usize,
    pub cursor_col: usize,
    pub content: Vector<Chunk>,
    pub max_line_length: usize,
    pub edit_mode: EditorMode,
}

impl Data for BufferState {
    fn same(&self, other: &Self) -> bool {
        self.name.same(&other.name)
            & self.line_length.same(&other.line_length)
            & self.client_offset.same(&other.client_offset)
            & self.cursor_line.same(&other.cursor_line)
            & self.cursor_col.same(&other.cursor_col)
            & (self.edit_mode == other.edit_mode)
            & (if self.content.is_inline() {
                self.content.len() == other.content.len()
                    && self
                        .content
                        .iter()
                        .zip(other.content.iter())
                        .all(|(a, b)| a == b)
            } else {
                self.content.ptr_eq(&other.content)
            })
    }
}

impl Default for BufferState {
    fn default() -> Self {
        Self {
            name: ArcStr::from("*scratch*"),
            line_length: 0,
            client_offset: 0,
            cursor_line: 0,
            cursor_col: 0,
            content: vector![],
            max_line_length: 0,
            edit_mode: EditorMode::Normal,
        }
    }
}

#[derive(Default, Clone, Data, Lens)]
struct AppState {
    buffer_state: BufferState,
    client_status: ClientStatus,
}

struct Delegate;

const BUFFER_STATE_UPDATE: Selector<BufferStatus> = Selector::new("reborgui.buffer_state_update");
const EDITOR_MODE: Selector<EditorMode> = Selector::new("reborgui.editor_mode");
const EDITOR_COMMAND: Selector<(String, usize)> = Selector::new("reborgui.editor_command");

impl AppDelegate<AppState> for Delegate {
    fn command(
        &mut self,
        _ctx: &mut DelegateCtx,
        _target: Target,
        cmd: &Command,
        data: &mut AppState,
        _env: &Env,
    ) -> Handled {
        if let Some(bs) = cmd.get(BUFFER_STATE_UPDATE) {
            data.buffer_state.content = Vector::from(bs.content.clone());
            data.buffer_state.line_length = bs.buf_line_length;
            data.buffer_state.client_offset = bs.client_offset;
            data.buffer_state.max_line_length = bs.max_line_length;
            data.buffer_state.cursor_line = bs.cursor_line;
            data.buffer_state.cursor_col = bs.cursor_col;
            return Handled::Yes;
        }
        if cmd.is(QUIT_APP) {
            Application::global().quit();
            return Handled::Yes;
        }
        if let Some(mode) = cmd.get(EDITOR_MODE) {
            data.client_status.edit_mode = mode.clone();
            data.buffer_state.edit_mode = mode.clone();
            return Handled::Yes;
        }
        if let Some((cmd, pos)) = cmd.get(EDITOR_COMMAND) {
            data.client_status.current_command = cmd.clone();
            data.client_status.cmd_pos = *pos;
            return Handled::Yes;
        }
        Handled::No
    }
}

pub struct GUI {
    term_size: (u16, u16),
    input_chan: Sender<Input>,
    input_handle: Option<JoinHandle<()>>,
}

impl UI for GUI {
    fn run(core_chan: Sender<Msg>) -> (Sender<Msg>, JoinHandle<()>) {
        let (ui_chan, recv_chan) = unbounded();

        let mut ui = GUI::new(core_chan, ui_chan.clone());
        let ui_handler = spawn(move || ui.mainloop(recv_chan));
        (ui_chan, ui_handler)
    }

    fn line_cache() -> usize {
        10
    }

    fn scratch_text() -> String {
        "\
This is a demo of a neat editor built with druid.

## Features:

- multiple cursors
  - no noticeable lag when editing 1000+ selections at once
- multiple selections
- smooth scrolling (tested with 100K+ lines)
- handle wide characters correctly (e.g. tabs)
- probably a lot of bugs, certainly some annoying glitches and expect panics

## Controls

At the moment the only available input handler is a (very small)
subset of Kakoune's bindings. It's quite good for the modal editing
with multiple selections model. If you're not familiar with Kakoune
think vim, but NORMAL and VISUAL are merged into a single mode. If you
don't know vim either, play with the editor or wait until we add a
non-modal input handler.

The GUI also has limited support of mouse controls:
- vertical scrolling
- the cursor follows the click.

### Normal mode

Cursor movements: `h`, `j`, `k`, `l`, and arrow keys
Extend the selections: `H`, `L`
End of line: `$`
Copy the selection to the next line: `C`
Enter the insert mode: `i`
Enter the insert mode at the begining of the current line: `I`
Enter the insert mode at the end of the current line: `A`
Enter the insert mode below the current line: `o`
Enter the insert mode above the current line: `O`
Enter the command mode: `:`
Enter the goto mode: `g`

### Insert mode

Exit to previous mode: `Esc`

### Command mode

Quit: `:q`, `:quit`

### Goto mode

Go to the first line: `g`

## Lets play

Just type: `ggCCCOHello World!<esc>^LLLLLLLHHi**<esc>l:q<ret>`

"
        .to_owned()
    }
}

impl GUI {
    fn new(core_chan: Sender<Msg>, ui_chan: Sender<Msg>) -> Self {
        // Create the InputHandler
        let (input_chan, input_handle) = InputHandler::run(core_chan, ui_chan);
        Self {
            term_size: (80, 24),
            input_chan,
            input_handle: Some(input_handle),
        }
    }

    fn build_ui(input_chan: Sender<Input>) -> impl Widget<AppState> {
        let bottom_bar = Flex::row()
            .with_spacer(4.0)
            .with_child(
                Label::dynamic(|data: &AppState, _| match data.client_status.edit_mode {
                    EditorMode::Insert => "INSERT".to_owned(),
                    EditorMode::Normal => "NORMAL".to_owned(),
                    EditorMode::Goto => "GOTO".to_owned(),
                    EditorMode::Command => format!(":{}", data.client_status.current_command),
                })
                .with_font(FontDescriptor::new(FONT).with_size(10.).with_weight(WEIGHT)),
            )
            .with_flex_spacer(10.0)
            .with_child(
                Label::dynamic(|data: &AppState, _| {
                    format!(
                        "{} {}:{} / {}",
                        data.buffer_state.name,
                        data.buffer_state.cursor_line + 1,
                        data.buffer_state.cursor_col + 1,
                        data.buffer_state.line_length
                    )
                })
                .with_font(FontDescriptor::new(FONT).with_size(10.).with_weight(WEIGHT)),
            )
            .with_spacer(10.0);

        let layout: Flex<AppState> = Flex::column()
            // Top bit is flex, as it fills all space.
            // Bottom bar is fixed width, so not flex
            .with_flex_child(
                ScrollEditWidget::new(input_chan).lens(AppState::buffer_state),
                1.0,
            )
            .with_child(bottom_bar);
        layout
        // layout.debug_paint_layout()
    }

    fn mainloop(&mut self, recv_chan: Receiver<Msg>) {
        let launcher =
            AppLauncher::with_window(WindowDesc::new(Self::build_ui(self.input_chan.clone())))
                .delegate(Delegate);
        let event_sink = launcher.get_external_handle();
        // Init client size
        // self.get_client_size();
        // listen to editor commands
        let editor_cmd_handler = spawn(move || {
            for order in recv_chan {
                match order {
                    Msg::BufferStatus(bs) => {
                        event_sink
                            .submit_command(BUFFER_STATE_UPDATE, bs, Target::Auto)
                            .unwrap();
                    }
                    Msg::Quit => {
                        event_sink
                            .submit_command(QUIT_APP, (), Target::Auto)
                            .unwrap();
                        break;
                    }
                    Msg::SetEditorMode(mode) => {
                        event_sink
                            .submit_command(EDITOR_MODE, mode, Target::Auto)
                            .unwrap();
                    }
                    Msg::SetEditorCommand(cmd, pos) => {
                        event_sink
                            .submit_command(EDITOR_COMMAND, (cmd, pos), Target::Auto)
                            .unwrap();
                    }
                    _ => (),
                }
            }
        });
        launcher.launch(AppState::default()).unwrap();
        editor_cmd_handler.join().unwrap();
    }
}
