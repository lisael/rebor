#! /bin/bash

set -eux

cargo build --release

BIN=../target/release/rebor

HERE=$(dirname $0)
cd "${HERE}"

FIFO=/tmp/rebor_functest_$(date +"%s")

# kill all subprocess at exit
trap "rm $FIFO; trap - SIGTERM && kill -- -$$" SIGINT SIGTERM EXIT

mkfifo "${FIFO}"
# cat > "${FIFO}" &

for input in $(ls | grep -e ".input$"); do
    eval "$BIN < $FIFO" &
    python - <<EOF
print("$FIFO")
print("$input")
with open("$input") as input:
    input = input.read()
    with open("$FIFO", "w") as fifo:
        for char in input:
            fifo.write(char)
            fifo.flush()
EOF
    reset
done
